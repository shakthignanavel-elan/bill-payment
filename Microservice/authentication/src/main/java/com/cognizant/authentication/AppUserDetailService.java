package com.cognizant.authentication;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.cognizant.authentication.model.AllUser;
import com.cognizant.authentication.model.Role;
import com.cognizant.authentication.model.User;
import com.cognizant.authentication.model.Vendor;
import com.cognizant.authentication.repository.AllUserRepository;
import com.cognizant.authentication.repository.UserRepository;
import com.cognizant.authentication.repository.VendorRepository;

@Service
public class AppUserDetailService implements UserDetailsService {

	@Autowired
	AllUserRepository allUserRepository;
	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	UserRepository userRepository;
	@Autowired
	VendorRepository vendorRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		AllUser allUser = allUserRepository.findByUserName(username);
		AppUser appUser = null;
		if (allUser != null) {
			appUser = new AppUser(allUser);
		} else {
			throw new UsernameNotFoundException(username);
		}
		return appUser;
	}

	@Transactional
	public void signup(User user) throws UserAlreadyExistsException {
		if (allUserRepository.findByUserName(user.getUsername()) == null) {
			Set<Role> userRole = new HashSet<Role>();
			userRole.add(new Role(2, "USER"));
			AllUser allUser = new AllUser();
			allUser.setUserName(user.getUsername());

			allUser.setRoleList(userRole);
			// user.setUserRole(userRole);
			String password = passwordEncoder.encode(user.getPassword());
			allUser.setUserPassword(password);
			user.setPassword(password);
			allUserRepository.save(allUser);
			userRepository.save(user);
		} else {
			throw new UserAlreadyExistsException();
		}

	}

	@Transactional
	public void signup(Vendor vendor) throws UserAlreadyExistsException {
		if (allUserRepository.findByUserName(vendor.getVendorRegNo()) == null) {
			Set<Role> userRole = new HashSet<Role>();
			userRole.add(new Role(3, "VENDOR"));
			AllUser allUser = new AllUser();
			allUser.setUserName(vendor.getVendorRegNo());

			allUser.setRoleList(userRole);
			// user.setUserRole(userRole);
			String password = passwordEncoder.encode(vendor.getPassword());
			allUser.setUserPassword(password);
			vendor.setPassword(password);
			allUserRepository.save(allUser);
			vendorRepository.save(vendor);
		} else {
			throw new UserAlreadyExistsException();
		}

	}

	@Transactional
	public User getUser(String userName) {
		return this.userRepository.findByUsername(userName);
	}

//	@Transactional
//	public List<AllUser> getUsers() {
//		return allUserRepository.findAll();
//	}
//
//	@Transactional
//	public User getUserByName(String name) {
//		return allUserRepository.findByUserName(name);
//	}

}
