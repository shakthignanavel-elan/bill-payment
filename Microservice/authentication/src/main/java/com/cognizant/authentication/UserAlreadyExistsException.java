package com.cognizant.authentication;

public class UserAlreadyExistsException extends Exception {
	public UserAlreadyExistsException() {
		super("User already exists");
	}
}
