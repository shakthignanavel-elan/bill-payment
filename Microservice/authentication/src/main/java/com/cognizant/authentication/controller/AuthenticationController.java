package com.cognizant.authentication.controller;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.authentication.AppUserDetailService;
import com.cognizant.authentication.AuthenticationApplication;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/bill-payment-authentication")
public class AuthenticationController {

	@Autowired
	AppUserDetailService appUserDetailService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationApplication.class);
	@GetMapping("/authenticate")
	private Map<String,String> authenticateUser(@RequestHeader("Authorization") String authHeader) {
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String user = authentication.getName();
		UserDetails userDetails = appUserDetailService.loadUserByUsername(user);
		String role = userDetails.getAuthorities().toArray()[0].toString();
		Map<String, String> newMap = new HashMap<String, String>();
		newMap.put("token", generateJwt(getUser(authHeader)));
		newMap.put("role", role);
		newMap.put("name", user);
		LOGGER.info("End");
		return newMap;
		
	}
	private String getUser(String authHeader) {
		LOGGER.info("Start");
		String[] encodedCredentials = authHeader.split("Basic ");
		String encodedMsg = "";
		if (encodedCredentials.length == 2) {
			encodedMsg = encodedCredentials[1];
		}
		String decodedMessage = new String(Base64.getDecoder().decode(encodedMsg));
		LOGGER.info("End");
		return decodedMessage.split(":")[0];
	}

	private String generateJwt(String user) {
		LOGGER.info("Start");
		JwtBuilder builder = Jwts.builder();
		builder.setSubject(user);
		builder.setIssuedAt(new Date());
		builder.setExpiration(new Date((new Date()).getTime() + 1200000));
		builder.signWith(SignatureAlgorithm.HS256, "secretkey");
		String token = builder.compact();
		LOGGER.info("End");
		return token;

	}
}
