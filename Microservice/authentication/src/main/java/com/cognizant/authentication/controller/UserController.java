package com.cognizant.authentication.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.authentication.AppUserDetailService;
import com.cognizant.authentication.AuthenticationApplication;
import com.cognizant.authentication.UserAlreadyExistsException;
import com.cognizant.authentication.model.User;
import com.cognizant.authentication.model.Vendor;

@RestController
@RequestMapping("/bill-payment-authentication")
public class UserController {
	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	AppUserDetailService appUserDetailsService;
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationApplication.class);

	@PostMapping("/users")
	public void signup(@RequestBody @Valid User users) throws UserAlreadyExistsException {
		LOGGER.info("Start");
		appUserDetailsService.signup(users);
		LOGGER.info("End");
	}
	@PostMapping("/vendors")
	public void signup(@RequestBody @Valid Vendor vendor) throws UserAlreadyExistsException {
		LOGGER.info("Start");
		appUserDetailsService.signup(vendor);
		LOGGER.info("End");
	}
	
	@GetMapping("/user/{userName}")
	public void getUser(@PathVariable String userName) {
		
	}
}