package com.cognizant.authentication.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bill_details_credit_card")
public class BillDetailsCreditCard {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bd_cc_id")
	private int id;
	@Column(name = "bd_cc_amount")
	private long amount;
	@Column(name = "bd_cc_date")
	private Date date;
	@Column(name = "bd_cc_payment_type")
	private String paymentType;
	@Column(name = "bd_cc_card_number")
	private long cardNumber;
	@Column(name = "bd_cc_frequency")
	private int frequency;
	@ManyToOne
	@JoinColumn(name = "bd_cc_us_id")
	private User user;
	@ManyToOne
	@JoinColumn(name = "bd_cc_ve_id")
	private Vendor vendor;

	public BillDetailsCreditCard() {
		super();
	}

	public BillDetailsCreditCard(int id, long amount, Date date, String paymentType, long cardNumber, int frequency,
			User user, Vendor vendor) {
		super();
		this.id = id;
		this.amount = amount;
		this.date = date;
		this.paymentType = paymentType;
		this.cardNumber = cardNumber;
		this.frequency = frequency;
		this.user = user;
		this.vendor = vendor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public long getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@Override
	public String toString() {
		return "BillDetailsCreditCard [id=" + id + ", amount=" + amount + ", date=" + date + ", paymentType="
				+ paymentType + ", cardNumber=" + cardNumber + ", frequency=" + frequency + ", user=" + user
				+ ", vendor=" + vendor + "]";
	}

		

}
