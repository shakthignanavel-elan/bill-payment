package com.cognizant.authentication.model;

import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bill_details_electricity")
public class BillDetailsElectricity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bd_el_id")
	private int id;
	@Column(name = "bd_el_amount")
	private long amount;
	@Column(name = "bd_el_date")
	private Date date;
	@Column(name = "bd_el_payment_type")
	private String paymentType;
	@Column(name = "bd_el_account_number")
	private String accountNumber;
	@Column(name = "bd_el_frequency")
	private int frequency;
	@ManyToOne
	@JoinColumn(name = "bd_el_us_id")
	private User user;
	@ManyToOne
	@JoinColumn(name = "bd_el_ve_id")
	private Vendor vendor;

	public BillDetailsElectricity() {
		super();
	}

	public BillDetailsElectricity(int id, long amount, Date date, String paymentType, String accountNumber,
			int frequency, User user, Vendor vendor) {
		super();
		this.id = id;
		this.amount = amount;
		this.date = date;
		this.paymentType = paymentType;
		this.accountNumber = accountNumber;
		this.frequency = frequency;
		this.user = user;
		this.vendor = vendor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@Override
	public String toString() {
		return "BillDetailsElectricity [id=" + id + ", amount=" + amount + ", date=" + date + ", paymentType="
				+ paymentType + ", accountNumber=" + accountNumber + ", frequency=" + frequency + ", user=" + user
				+ ", vendor=" + vendor + "]";
	}

}
