package com.cognizant.authentication.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bill_details_insurance")
public class BillDetailsInsurance {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bd_in_id")
	private int id;
	@Column(name = "bd_in_amount")
	private long amount;
	@Column(name = "bd_in_date")
	private Date date;
	@Column(name = "bd_in_policy_number")
	private long policyNumber;
	@Column(name = "bd_in_date_of_birth")
	private Date dateOfBirth;
	@Column(name = "bd_in_account_name")
	private String accountName;
	@Column(name = "bd_in_frequency")
	private int frequency;
	@ManyToOne
	@JoinColumn(name = "bd_in_us_id")
	private User user;
	@ManyToOne
	@JoinColumn(name = "bd_in_ve_id")
	private Vendor vendor;

	public BillDetailsInsurance() {
		super();
	}

	public BillDetailsInsurance(int id, long amount, Date date, long policyNumber, Date dateOfBirth, String accountName,
			int frequency, User user, Vendor vendor) {
		super();
		this.id = id;
		this.amount = amount;
		this.date = date;
		this.policyNumber = policyNumber;
		this.dateOfBirth = dateOfBirth;
		this.accountName = accountName;
		this.frequency = frequency;
		this.user = user;
		this.vendor = vendor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public long getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(long policyNumber) {
		this.policyNumber = policyNumber;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@Override
	public String toString() {
		return "BillDetailsInsurance [id=" + id + ", amount=" + amount + ", date=" + date + ", policyNumber="
				+ policyNumber + ", dateOfBirth=" + dateOfBirth + ", accountName=" + accountName + ", frequency="
				+ frequency + ", user=" + user + ", vendor=" + vendor + "]";
	}

}
