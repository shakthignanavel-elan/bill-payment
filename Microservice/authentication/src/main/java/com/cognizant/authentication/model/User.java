package com.cognizant.authentication.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Table(name = "user")
public class User {
	@Id
	@Column(name = "us_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull
	private int id;
	@Column(name = "us_user_name")
	private String username;
	@Column(name = "us_first_name")
	private String firstName;
	@Column(name = "us_last_name")
	private String lastName;
	@Column(name = "us_age")
	private int age;
	@Column(name = "us_gender")
	private String gender;
	@Column(name = "us_contact_number")
	private long contact;
	@Column(name = "us_pan_number")
	private String panNumber;
	@Column(name = "us_aadhar_number")
	private String adhaarNumber;
	@Column(name = "us_password")
	private String password;
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@NotNull
	private Set<BillDetailsCreditCard> BillDetailsCreditCard;
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@NotNull
	private Set<BillDetailsDth> BillDetailsDth;
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@NotNull
	private Set<BillDetailsElectricity> BillDetailsElectricity;
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@NotNull
	private Set<BillDetailsInsurance> BillDetailsInsurance;
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@NotNull
	private Set<BillDetailsLoanAccount> BillDetailsLoanAccount;
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@NotNull
	private Set<BillDetailsTax> BillDetailsTax;
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@NotNull
	private Set<BillDetailsTelephone> BillDetailsTelephone;

	public User() {
		super();
	}

	public User(int id, String username, String firstName, String lastName, int age, String gender, long contact,
			String panNumber, String adhaarNumber, String password, Set<BillDetailsCreditCard> creditCardList,
			Set<BillDetailsDth> dthList, Set<BillDetailsElectricity> electricityList,
			Set<BillDetailsInsurance> insuranceList, Set<BillDetailsLoanAccount> loanAccountList,
			Set<BillDetailsTax> taxList, Set<BillDetailsTelephone> telephoneList) {
		super();
		this.id = id;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.gender = gender;
		this.contact = contact;
		this.panNumber = panNumber;
		this.adhaarNumber = adhaarNumber;
		this.password = password;
		this.BillDetailsCreditCard = creditCardList;
		this.BillDetailsDth = dthList;
		this.BillDetailsElectricity = electricityList;
		this.BillDetailsInsurance = insuranceList;
		this.BillDetailsLoanAccount = loanAccountList;
		this.BillDetailsTax = taxList;
		this.BillDetailsTelephone = telephoneList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getContact() {
		return contact;
	}

	public void setContact(long contact) {
		this.contact = contact;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getAdhaarNumber() {
		return adhaarNumber;
	}

	public void setAdhaarNumber(String adhaarNumber) {
		this.adhaarNumber = adhaarNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<BillDetailsCreditCard> getCreditCardList() {
		return BillDetailsCreditCard;
	}

	public void setCreditCardList(Set<BillDetailsCreditCard> creditCardList) {
		this.BillDetailsCreditCard = creditCardList;
	}

	public Set<BillDetailsDth> getDthList() {
		return BillDetailsDth;
	}

	public void setDthList(Set<BillDetailsDth> dthList) {
		this.BillDetailsDth = dthList;
	}

	public Set<BillDetailsElectricity> getElectricityList() {
		return BillDetailsElectricity;
	}

	public void setElectricityList(Set<BillDetailsElectricity> electricityList) {
		this.BillDetailsElectricity = electricityList;
	}

	public Set<BillDetailsInsurance> getInsuranceList() {
		return BillDetailsInsurance;
	}

	public void setInsuranceList(Set<BillDetailsInsurance> insuranceList) {
		this.BillDetailsInsurance = insuranceList;
	}

	public Set<BillDetailsLoanAccount> getLoanAccountList() {
		return BillDetailsLoanAccount;
	}

	public void setLoanAccountList(Set<BillDetailsLoanAccount> loanAccountList) {
		this.BillDetailsLoanAccount = loanAccountList;
	}

	public Set<BillDetailsTax> getTaxList() {
		return BillDetailsTax;
	}

	public void setTaxList(Set<BillDetailsTax> taxList) {
		this.BillDetailsTax = taxList;
	}

	public Set<BillDetailsTelephone> getTelephoneList() {
		return BillDetailsTelephone;
	}

	public void setTelephoneList(Set<BillDetailsTelephone> telephoneList) {
		this.BillDetailsTelephone = telephoneList;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", age=" + age + ", gender=" + gender + ", contact=" + contact + ", panNumber=" + panNumber
				+ ", adhaarNumber=" + adhaarNumber + ", password=" + password + ", creditCardList=" + BillDetailsCreditCard
				+ ", dthList=" + BillDetailsDth + ", electricityList=" + BillDetailsElectricity + ", insuranceList=" + BillDetailsInsurance
				+ ", loanAccountList=" + BillDetailsLoanAccount + ", taxList=" + BillDetailsTax + ", telephoneList=" + BillDetailsTelephone
				+ "]";
	}

}
