package com.cognizant.authentication.model;

public class UserDTO {
	private User user;
	private int userID;

	public UserDTO() {
		super();
	}

	public UserDTO(User user, int userID) {
		super();
		this.user = user;
		this.userID = userID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "UserDTO [user=" + user + ", userID=" + userID + "]";
	}

}
