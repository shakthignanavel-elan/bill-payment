package com.cognizant.authentication.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="vendor")
public class Vendor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ve_ID")
	private int vendorId;
	@Column(name="ve_name")
	private String vendorName;
	@Column(name="ve_company_register_number")
	private String vendorRegNo;
	@Column(name="ve_type")
	private String vendorType;
	@Column(name="ve_address")
	private String address;
	@Column(name="ve_country")
	private String country;
	@Column(name="ve_state")
	private String state;
	@Column(name="ve_email")
	private String email;
	@Column(name="ve_contact_number")
	private long contactNumber;
	@Column(name="ve_website")
	private String website;
	@Column(name="ve_certificate_issued_date")
	private Date issuedDate;
	@Column(name="ve_certificate_validity_date")
	private Date validityDate;
	@Column(name="ve_year_of_establish")
	private int establishedYear;
	@Column(name="ve_payment_gateway")
	private String gateway;
	@Column(name="ve_password")
	private String password;
	@Column(name="ve_image")
	private String imageURL;
	@Column(name="ve_status")
	private int status;
	@Column(name="ve_remark")
	private String remark;
	public Vendor() {
		super();
	}
	public Vendor(int vendorId, String vendorName, String vendorRegNo, String vendorType, String address,
			String country, String state, String email, long contactNumber, String website, Date issuedDate,
			Date validityDate, int establishedYear, String gateway, String password, String imageURL, int status) {
		super();
		this.vendorId = vendorId;
		this.vendorName = vendorName;
		this.vendorRegNo = vendorRegNo;
		this.vendorType = vendorType;
		this.address = address;
		this.country = country;
		this.state = state;
		this.email = email;
		this.contactNumber = contactNumber;
		this.website = website;
		this.issuedDate = issuedDate;
		this.validityDate = validityDate;
		this.establishedYear = establishedYear;
		this.gateway = gateway;
		this.password = password;
		this.imageURL = imageURL;
		this.status = status;
	}
	public int getVendorId() {
		return vendorId;
	}
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorRegNo() {
		return vendorRegNo;
	}
	public void setVendorRegNo(String vendorRegNo) {
		this.vendorRegNo = vendorRegNo;
	}
	public String getVendorType() {
		return vendorType;
	}
	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public Date getIssuedDate() {
		return issuedDate;
	}
	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}
	public Date getValidityDate() {
		return validityDate;
	}
	public void setValidityDate(Date validityDate) {
		this.validityDate = validityDate;
	}
	public int getEstablishedYear() {
		return establishedYear;
	}
	public void setEstablishedYear(int establishedYear) {
		this.establishedYear = establishedYear;
	}
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Vendor [vendorId=" + vendorId + ", vendorName=" + vendorName + ", vendorRegNo=" + vendorRegNo
				+ ", vendorType=" + vendorType + ", address=" + address + ", country=" + country + ", state=" + state
				+ ", email=" + email + ", contactNumber=" + contactNumber + ", website=" + website + ", issuedDate="
				+ issuedDate + ", validityDate=" + validityDate + ", establishedYear=" + establishedYear + ", gateway="
				+ gateway + ", password=" + password + ", imageURL=" + imageURL + ", status=" + status + "]";
	}
	
}
