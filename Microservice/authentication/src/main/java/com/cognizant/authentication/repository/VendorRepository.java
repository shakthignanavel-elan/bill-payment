package com.cognizant.authentication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cognizant.authentication.model.User;
import com.cognizant.authentication.model.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, String> {

	Vendor findByVendorId(int vendorId);

	Vendor findByVendorRegNo(String vendorRegNo);
}
