package com.cognizant.billpay;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.cognizant.billpay.model.AllUser;

public class AppUser implements UserDetails {

	private AllUser user;
	private Collection<? extends GrantedAuthority> authorities;

	public AppUser() {
		super();
	}

	public AppUser(AllUser user) {
		super();
		this.user = user;
		this.authorities = user.getRoleList().stream().map(role -> new SimpleGrantedAuthority(role.getRoleName()))
				.collect(Collectors.toList());
	}

	public AllUser getUser() {
		return user;
	}

	public void setUser(AllUser user) {
		this.user = user;
	}

	public Collection<? extends GrantedAuthority> getAuthoritites() {
		return authorities;
	}

	public void setAuthoritites(Collection<? extends GrantedAuthority> authoritites) {
		this.authorities = authoritites;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return this.authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return this.user.getUserPassword();
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return this.user.getUserName();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
