package com.cognizant.billpay;

import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.cognizant.billpay.model.AllUser;
import com.cognizant.billpay.model.Role;
import com.cognizant.billpay.model.User;
import com.cognizant.billpay.model.Vendor;
import com.cognizant.billpay.repository.AllUserRepository;
import com.cognizant.billpay.repository.UserRepository;
import com.cognizant.billpay.repository.VendorRepository;

@Service
public class AppUserDetailService implements UserDetailsService {

	@Autowired
	AllUserRepository allUserRepository;
	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	UserRepository userRepository;
	@Autowired
	VendorRepository vendorRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		AllUser allUser = allUserRepository.findByUserName(username);
		AppUser appUser = null;
		if (allUser != null) {
			appUser = new AppUser(allUser);
		} else {
			throw new UsernameNotFoundException(username);
		}
		return appUser;
	}


}
