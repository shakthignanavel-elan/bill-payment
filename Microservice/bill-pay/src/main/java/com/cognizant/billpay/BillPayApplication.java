package com.cognizant.billpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class BillPayApplication {

	public static void main(String[] args) {
		SpringApplication.run(BillPayApplication.class, args);
	}

}
