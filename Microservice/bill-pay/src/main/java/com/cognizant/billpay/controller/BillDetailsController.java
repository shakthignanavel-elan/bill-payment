package com.cognizant.billpay.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.billpay.model.BillDetailsCreditCard;
import com.cognizant.billpay.model.BillDetailsDth;
import com.cognizant.billpay.model.BillDetailsElectricity;
import com.cognizant.billpay.model.BillDetailsInsurance;
import com.cognizant.billpay.model.BillDetailsLoanAccount;
import com.cognizant.billpay.model.BillDetailsTax;
import com.cognizant.billpay.model.BillDetailsTelephone;
import com.cognizant.billpay.model.UserDTO;
import com.cognizant.billpay.service.BillDetailsCreditCardService;
import com.cognizant.billpay.service.BillDetailsDthService;
import com.cognizant.billpay.service.BillDetailsElectricityService;
import com.cognizant.billpay.service.BillDetailsInsuranceService;
import com.cognizant.billpay.service.BillDetailsLoanService;
import com.cognizant.billpay.service.BillDetailsTaxService;
import com.cognizant.billpay.service.BillDetailsTelephoneService;
import com.cognizant.billpay.service.UserService;

@RestController
@RequestMapping("/bill-payment/bill-details")
public class BillDetailsController {

	@Autowired
	BillDetailsCreditCardService billDetailsCreditCardService;
	@Autowired
	BillDetailsDthService billDetailsDthService;
	@Autowired
	BillDetailsElectricityService billDetailsElectricityService;
	@Autowired
	BillDetailsInsuranceService billDetailsInsuranceService;
	@Autowired
	BillDetailsLoanService billDetailsLoanService;
	@Autowired
	BillDetailsTaxService billDetailsTaxService;
	@Autowired
	BillDetailsTelephoneService billDetailsTelephoneService;
	@Autowired
	UserService userService;

	@RequestMapping("/getList/{userID}")
	public UserDTO getBillDetails(@PathVariable int userID) {
		UserDTO userDTO = new UserDTO();
		userDTO.setCreditCardBillList(billDetailsCreditCardService.getByUserID(userID));
		userDTO.setDthBillDetails(billDetailsDthService.getByUserID(userID));
		userDTO.setElectricityDetails(billDetailsElectricityService.getByUserID(userID));
		userDTO.setInsuranceBillDetails(billDetailsInsuranceService.getByUserID(userID));
		userDTO.setLoanBillList(billDetailsLoanService.getByUserID(userID));
		userDTO.setTaxBillList(billDetailsTaxService.getByUserID(userID));
		userDTO.setTelephoneBillList(billDetailsTelephoneService.getByUserID(userID));
		return userDTO;
	}

	@PostMapping("/updateList/credit-card")
	public void saveBill(@RequestBody BillDetailsCreditCard billDetailsCreditCard) {
		this.billDetailsCreditCardService.save(billDetailsCreditCard);
	}

	@PostMapping("/updateList/dth")
	public void saveBill(@RequestBody BillDetailsDth billDetailsDth) {
		this.billDetailsDthService.save(billDetailsDth);
	}

	@PostMapping("/updateList/electricity")
	public void saveBill(@RequestBody BillDetailsElectricity billDetailsElectricity) {
		this.billDetailsElectricityService.save(billDetailsElectricity);
	}

	@PostMapping("/updateList/insurance")
	public void saveBill(@RequestBody BillDetailsInsurance billDetailsInsurance) {
		this.billDetailsInsuranceService.save(billDetailsInsurance);
	}

	@PostMapping("/updateList/loan")
	public void saveBill(@RequestBody BillDetailsLoanAccount billDetailsLoanAccount) {
		this.billDetailsLoanService.save(billDetailsLoanAccount);
	}

	@PostMapping("/updateList/tax")
	public void saveBill(@RequestBody BillDetailsTax billDetailsTax) {
		this.billDetailsTaxService.save(billDetailsTax);
	}

	@PostMapping("/updateList/telephone")
	public void saveBill(@RequestBody BillDetailsTelephone billDetailsTelephone) {
		this.billDetailsTelephoneService.save(billDetailsTelephone);
	}
}
