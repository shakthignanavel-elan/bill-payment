package com.cognizant.billpay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.billpay.model.User;
import com.cognizant.billpay.model.UserDTO;
import com.cognizant.billpay.service.UserService;

@RestController
@RequestMapping("/bill-payment")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping("/user/{userName}")
	User getAllVendors(@PathVariable String userName) {
		return userService.getUserDetails(userName);
	}
}
