package com.cognizant.billpay.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.billpay.model.Vendor;
import com.cognizant.billpay.service.VendorService;

@RestController
@RequestMapping("/bill-payment")
public class VendorController {

	@Autowired
	VendorService vendorService;

	@GetMapping("/vendorList")
	List<Vendor> getAllVendors() {
		return vendorService.getAllVendors();
	}

	@PutMapping("/update-vendor-status/{vendor}")
	void updateVendorStatus(@PathVariable Vendor vendor) {
		System.out.println(vendor);
		vendorService.updateVendorStatus(vendor);
	}

	@GetMapping("/vendor/{registerNumber}")
	List<Vendor> getVendor(@PathVariable String registerNumber) {
		return vendorService.getVendor(registerNumber);
	}
	
	@GetMapping("/vendorTypes/{vendorType}")
	List<Vendor> getVendorOnType(@PathVariable String vendorType) {
		return vendorService.getVendorOnType(vendorType);
	}
	@PutMapping("/update-vendor")
	void updateVendor(@RequestBody Vendor vendor) {
		this.vendorService.updateVendor(vendor);
	}
}
