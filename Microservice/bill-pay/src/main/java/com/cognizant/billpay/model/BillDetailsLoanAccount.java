package com.cognizant.billpay.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "bill_details_loan_account")
public class BillDetailsLoanAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bd_la_id")
	private int id;
	@Column(name = "bd_la_amount")
	private long amount;
	@Column(name = "bd_la_date")
	private Date date;
	@Column(name = "bd_la_payment_type")
	private String paymentType;
	@Column(name = "bd_la_account_number")
	private long accountNumber;
	@Column(name = "bd_la_account_name")
	private String accountName;
	@Column(name = "bd_la_frequency")
	private int frequency;
	@ManyToOne
	@JoinColumn(name = "bd_la_us_id")
	private User user;
	@ManyToOne
	@JoinColumn(name = "bd_la_ve_id")
	private Vendor vendor;

	public BillDetailsLoanAccount() {
		super();
	}

	public BillDetailsLoanAccount(int id, long amount, Date date, String paymentType, long accountNumber,
			String accountName, int frequency, User user, Vendor vendor) {
		super();
		this.id = id;
		this.amount = amount;
		this.date = date;
		this.paymentType = paymentType;
		this.accountNumber = accountNumber;
		this.accountName = accountName;
		this.frequency = frequency;
		this.user = user;
		this.vendor = vendor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@Override
	public String toString() {
		return "BillDetailsLoanAccount [id=" + id + ", amount=" + amount + ", date=" + date + ", paymentType="
				+ paymentType + ", accountNumber=" + accountNumber + ", accountName=" + accountName + ", frequency="
				+ frequency + ", user=" + user.getId() + ", vendor=" + vendor.getVendorId() + "]";
	}

}
