package com.cognizant.billpay.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "bill_details_tax")
public class BillDetailsTax {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bd_ta_id")
	private int id;
	@Column(name = "bd_ta_amount")
	private long amount;
	@Column(name = "bd_ta_date")
	private Date date;
	@Column(name = "bd_ta_payment_type")
	private String paymentType;
	@Column(name = "bd_ta_pan_number")
	private String panNumber;
	@Column(name = "bd_ta_frequency")
	private int frequency;
	@ManyToOne
	@JoinColumn(name = "bd_ta_us_id")
	private User user;
	@ManyToOne
	@JoinColumn(name = "bd_ta_ve_id")
	private Vendor vendor;

	public BillDetailsTax() {
		super();
	}

	public BillDetailsTax(int id, long maount, Date date, String paymentType, String panNumber, int frequency,
			User user, Vendor vendor) {
		super();
		this.id = id;
		this.amount = maount;
		this.date = date;
		this.paymentType = paymentType;
		this.panNumber = panNumber;
		this.frequency = frequency;
		this.user = user;
		this.vendor = vendor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getMaount() {
		return amount;
	}

	public void setMaount(long maount) {
		this.amount = maount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@Override
	public String toString() {
		return "BillDetailsTax [id=" + id + ", maount=" + amount + ", date=" + date + ", paymentType=" + paymentType
				+ ", panNumber=" + panNumber + ", frequency=" + frequency + ", vendor=" + vendor.getVendorId()
				+ "]";
	}

}
