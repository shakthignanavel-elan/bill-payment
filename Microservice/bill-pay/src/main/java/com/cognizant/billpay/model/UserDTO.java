package com.cognizant.billpay.model;

import java.util.ArrayList;
import java.util.List;

public class UserDTO {

	private List<BillDetailsLoanAccount> loanBillList;
	private List<BillDetailsTax> taxBillList;
	private List<BillDetailsTelephone> telephoneBillList;
	private List<BillDetailsCreditCard> creditCardBillList;
	private List<BillDetailsDth> dthBillDetails;
	private List<BillDetailsElectricity> electricityDetails;
	private List<BillDetailsInsurance> insuranceBillDetails;

	public UserDTO() {
		super();
	}

	public UserDTO(List<BillDetailsLoanAccount> loanBillList, List<BillDetailsTax> taxBillList,
			List<BillDetailsTelephone> telephoneBillList, List<BillDetailsCreditCard> creditCardBillList,
			List<BillDetailsDth> dthBillDetails, List<BillDetailsElectricity> electricityDetails,
			List<BillDetailsInsurance> insuranceBillDetails) {
		super();
		this.loanBillList = loanBillList;
		this.taxBillList = taxBillList;
		this.telephoneBillList = telephoneBillList;
		this.creditCardBillList = creditCardBillList;
		this.dthBillDetails = dthBillDetails;
		this.electricityDetails = electricityDetails;
		this.insuranceBillDetails = insuranceBillDetails;
	}

	public List<BillDetailsLoanAccount> getLoanBillList() {
		return loanBillList;
	}

	public void setLoanBillList(List<BillDetailsLoanAccount> loanBillList) {
		this.loanBillList = loanBillList;
	}

	public List<BillDetailsTax> getTaxBillList() {
		return taxBillList;
	}

	public void setTaxBillList(List<BillDetailsTax> taxBillList) {
		this.taxBillList = taxBillList;
	}

	public List<BillDetailsTelephone> getTelephoneBillList() {
		return telephoneBillList;
	}

	public void setTelephoneBillList(List<BillDetailsTelephone> telephoneBillList) {
		this.telephoneBillList = telephoneBillList;
	}

	public List<BillDetailsCreditCard> getCreditCardBillList() {
		return creditCardBillList;
	}

	public void setCreditCardBillList(List<BillDetailsCreditCard> creditCardBillList) {
		this.creditCardBillList = creditCardBillList;
	}

	public List<BillDetailsDth> getDthBillDetails() {
		return dthBillDetails;
	}

	public void setDthBillDetails(List<BillDetailsDth> dthBillDetails) {
		this.dthBillDetails = dthBillDetails;
	}

	public List<BillDetailsElectricity> getElectricityDetails() {
		return electricityDetails;
	}

	public void setElectricityDetails(List<BillDetailsElectricity> electricityDetails) {
		this.electricityDetails = electricityDetails;
	}

	public List<BillDetailsInsurance> getInsuranceBillDetails() {
		return insuranceBillDetails;
	}

	public void setInsuranceBillDetails(List<BillDetailsInsurance> insuranceBillDetails) {
		this.insuranceBillDetails = insuranceBillDetails;
	}

	@Override
	public String toString() {
		return "UserDTO [loanBillList=" + loanBillList + ", taxBillList=" + taxBillList + ", telephoneBillList="
				+ telephoneBillList + ", creditCardBillList=" + creditCardBillList + ", dthBillDetails="
				+ dthBillDetails + ", electricityDetails=" + electricityDetails + ", insuranceBillDetails="
				+ insuranceBillDetails + "]";
	}

}
