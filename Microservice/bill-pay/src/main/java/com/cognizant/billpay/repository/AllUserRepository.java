package com.cognizant.billpay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cognizant.billpay.model.AllUser;

@Repository
public interface AllUserRepository extends JpaRepository<AllUser, String> {
	AllUser findByUserId(int userId);

	AllUser findByUserName(String userName);
}
