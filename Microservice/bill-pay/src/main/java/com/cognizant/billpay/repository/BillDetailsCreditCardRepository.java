package com.cognizant.billpay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.billpay.model.BillDetailsCreditCard;
import com.cognizant.billpay.model.BillDetailsElectricity;
@Repository
public interface BillDetailsCreditCardRepository extends JpaRepository<BillDetailsCreditCard, String> {

	@Query(value="select * from bill_details_credit_card where bd_cc_us_id=?1",nativeQuery = true)
	public List<BillDetailsCreditCard> getByUserID(int id);
}
