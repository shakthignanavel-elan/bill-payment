package com.cognizant.billpay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.billpay.model.BillDetailsCreditCard;
import com.cognizant.billpay.model.BillDetailsDth;
@Repository
public interface BillDetailsDthRepository extends JpaRepository<BillDetailsDth,String> {

	@Query(value="select * from bill_details_dth where bd_dt_us_id=?1",nativeQuery = true)
	public List<BillDetailsDth> getByUserID(int id);
}
