package com.cognizant.billpay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.billpay.model.BillDetailsCreditCard;
import com.cognizant.billpay.model.BillDetailsElectricity;
@Repository
public interface BillDetailsElectricityRepository extends JpaRepository<BillDetailsElectricity,String>{

	//@Query("select billDetailsElectricity from BillDetailsElectricity billDetailsElectricity where billDetailsElectricity.user.id=?1")
	@Query(value="select * from bill_details_electricity where bd_el_us_id=?1",nativeQuery = true)
	public List<BillDetailsElectricity> getByUserID(int id);
}
