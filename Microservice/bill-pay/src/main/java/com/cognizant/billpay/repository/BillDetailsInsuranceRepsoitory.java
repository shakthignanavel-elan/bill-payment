package com.cognizant.billpay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.billpay.model.BillDetailsElectricity;
import com.cognizant.billpay.model.BillDetailsInsurance;
@Repository
public interface BillDetailsInsuranceRepsoitory extends JpaRepository<BillDetailsInsurance, String>{

	@Query(value="select * from bill_details_insurance where bd_in_us_id=?1",nativeQuery = true)
	public List<BillDetailsInsurance> getByUserID(int id);
}
