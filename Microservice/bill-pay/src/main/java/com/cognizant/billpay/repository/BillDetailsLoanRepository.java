package com.cognizant.billpay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.billpay.model.BillDetailsInsurance;
import com.cognizant.billpay.model.BillDetailsLoanAccount;
@Repository
public interface BillDetailsLoanRepository extends JpaRepository<BillDetailsLoanAccount,String> {

	@Query(value="select * from bill_details_loan_account where bd_la_us_id=?1",nativeQuery = true)
	public List<BillDetailsLoanAccount> getByUserID(int id);
}
