package com.cognizant.billpay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.billpay.model.BillDetailsLoanAccount;
import com.cognizant.billpay.model.BillDetailsTax;
@Repository
public interface BillDetailsTaxRepsitory extends JpaRepository<BillDetailsTax, String>{

	@Query(value="select * from bill_details_tax where bd_ta_us_id=?1",nativeQuery = true)
	public List<BillDetailsTax> getByUserID(int id);
}
