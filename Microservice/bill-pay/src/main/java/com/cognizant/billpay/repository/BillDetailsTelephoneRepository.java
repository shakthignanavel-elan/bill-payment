package com.cognizant.billpay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.billpay.model.BillDetailsTax;
import com.cognizant.billpay.model.BillDetailsTelephone;
@Repository
public interface BillDetailsTelephoneRepository extends JpaRepository<BillDetailsTelephone, String> {

	@Query(value="select * from bill_details_telephone where bd_te_us_id=?1",nativeQuery = true)
	public List<BillDetailsTelephone> getByUserID(int id);
}
