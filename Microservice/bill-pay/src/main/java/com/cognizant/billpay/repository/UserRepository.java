package com.cognizant.billpay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.billpay.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

	User findById(int userId);

	@Query("select user from User user where user.username=?1")
	User getUser(String userName);
}
