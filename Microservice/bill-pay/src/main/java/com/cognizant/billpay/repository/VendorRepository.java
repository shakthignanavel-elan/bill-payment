package com.cognizant.billpay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.billpay.model.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, String> {

	Vendor findByVendorId(int vendorId);

	@Query("select vendor from Vendor vendor where vendor.vendorRegNo=?1")
	List<Vendor> findByVendorRegNo(String vendorRegNo);
	
	@Query("select vendorId from Vendor vendor where vendor.vendorRegNo=?1")
	int findVendorId(String vendorRegNo);
	
	@Query("select vendor from Vendor vendor where vendor.status=1 AND vendor.vendorType=?1")
	List<Vendor> findVendorOnType(String type);
}
