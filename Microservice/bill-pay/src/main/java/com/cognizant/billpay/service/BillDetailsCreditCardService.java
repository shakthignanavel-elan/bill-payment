package com.cognizant.billpay.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.billpay.model.BillDetailsCreditCard;
import com.cognizant.billpay.repository.BillDetailsCreditCardRepository;

@Service
public class BillDetailsCreditCardService {

	@Autowired
	BillDetailsCreditCardRepository billDetailsCreditCardRepository;
	
	@Transactional
	public List<BillDetailsCreditCard> getByUserID(int userId) {
		return billDetailsCreditCardRepository.getByUserID(userId);
	}
	@Transactional
	public void save(BillDetailsCreditCard billDetailsCreditCard) {
		billDetailsCreditCardRepository.save(billDetailsCreditCard);
	}
}
