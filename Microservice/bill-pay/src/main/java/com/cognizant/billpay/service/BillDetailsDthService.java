package com.cognizant.billpay.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.billpay.model.BillDetailsCreditCard;
import com.cognizant.billpay.model.BillDetailsDth;
import com.cognizant.billpay.repository.BillDetailsCreditCardRepository;
import com.cognizant.billpay.repository.BillDetailsDthRepository;

@Service
public class BillDetailsDthService {

	@Autowired
	BillDetailsDthRepository billDetailsDthRepository;
	
	public List<BillDetailsDth> getByUserID(int userId) {
		return billDetailsDthRepository.getByUserID(userId);
	}
	@Transactional
	public void save(BillDetailsDth billDetailsDth) {
		billDetailsDthRepository.save(billDetailsDth);
	}
}
