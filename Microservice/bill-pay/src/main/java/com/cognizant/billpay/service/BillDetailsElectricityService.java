package com.cognizant.billpay.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.billpay.model.BillDetailsDth;
import com.cognizant.billpay.model.BillDetailsElectricity;
import com.cognizant.billpay.repository.BillDetailsElectricityRepository;

@Service
public class BillDetailsElectricityService {

	@Autowired
	BillDetailsElectricityRepository billDetailsElectricityRepository;
	
	public List<BillDetailsElectricity> getByUserID(int userID) {
		return billDetailsElectricityRepository.getByUserID(userID);
	}
	@Transactional
	public void save(BillDetailsElectricity billDetailsElectricity) {
		billDetailsElectricityRepository.save(billDetailsElectricity);
	}
}
