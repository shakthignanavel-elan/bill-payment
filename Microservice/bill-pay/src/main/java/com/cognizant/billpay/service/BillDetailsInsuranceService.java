package com.cognizant.billpay.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.billpay.model.BillDetailsElectricity;
import com.cognizant.billpay.model.BillDetailsInsurance;
import com.cognizant.billpay.repository.BillDetailsElectricityRepository;
import com.cognizant.billpay.repository.BillDetailsInsuranceRepsoitory;

@Service
public class BillDetailsInsuranceService {

	@Autowired
	BillDetailsInsuranceRepsoitory billDetailsInsuranceRepsoitory;
	
	public List<BillDetailsInsurance> getByUserID(int userID) {
		return billDetailsInsuranceRepsoitory.getByUserID(userID);
	}
	@Transactional
	public void save(BillDetailsInsurance billDetailsInsurance) {
		billDetailsInsuranceRepsoitory.save(billDetailsInsurance);
	}
}
