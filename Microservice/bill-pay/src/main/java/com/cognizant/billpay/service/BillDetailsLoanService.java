package com.cognizant.billpay.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.billpay.model.BillDetailsInsurance;
import com.cognizant.billpay.model.BillDetailsLoanAccount;
import com.cognizant.billpay.repository.BillDetailsInsuranceRepsoitory;
import com.cognizant.billpay.repository.BillDetailsLoanRepository;

@Service
public class BillDetailsLoanService {

	@Autowired
	BillDetailsLoanRepository billDetailsLoanRepository;
	
	public List<BillDetailsLoanAccount> getByUserID(int userID) {
		return billDetailsLoanRepository.getByUserID(userID);
	}
	@Transactional
	public void save(BillDetailsLoanAccount billDetailsLoanAccount) {
		billDetailsLoanRepository.save(billDetailsLoanAccount);
	}
}
