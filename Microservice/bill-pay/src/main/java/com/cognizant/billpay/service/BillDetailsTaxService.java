package com.cognizant.billpay.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.billpay.model.BillDetailsLoanAccount;
import com.cognizant.billpay.model.BillDetailsTax;
import com.cognizant.billpay.repository.BillDetailsLoanRepository;
import com.cognizant.billpay.repository.BillDetailsTaxRepsitory;

@Service
public class BillDetailsTaxService {

	@Autowired
	BillDetailsTaxRepsitory billDetailsTaxRepsitory;
	
	public List<BillDetailsTax> getByUserID(int userID) {
		return billDetailsTaxRepsitory.getByUserID(userID);
	}
	@Transactional
	public void save(BillDetailsTax billDetailsTax) {
		billDetailsTaxRepsitory.save(billDetailsTax);
	}
}
