package com.cognizant.billpay.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.billpay.model.BillDetailsTax;
import com.cognizant.billpay.model.BillDetailsTelephone;
import com.cognizant.billpay.repository.BillDetailsTaxRepsitory;
import com.cognizant.billpay.repository.BillDetailsTelephoneRepository;

@Service
public class BillDetailsTelephoneService {
	@Autowired
	BillDetailsTelephoneRepository billDetailsTelephoneRepository;

	public List<BillDetailsTelephone> getByUserID(int userID) {
		return billDetailsTelephoneRepository.getByUserID(userID);
	}

	@Transactional
	public void save(BillDetailsTelephone billDetailsTelephone) {
		billDetailsTelephoneRepository.save(billDetailsTelephone);
	}
}
