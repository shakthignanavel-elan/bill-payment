package com.cognizant.billpay.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.billpay.model.User;
import com.cognizant.billpay.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

//	@Transactional
//	public UserDTO getUserDetails(String userName) {
//		User user = this.userRepository.findByUsername(userName);
//		UserDTO userDTO = new UserDTO();
//		userDTO.setUser(user);
//		userDTO.setServiceNotification(new ArrayList<String>());
//		for (BillDetailsCreditCard billDetails : user.getCreditCardList()) {
//			if (Days.daysBetween(new DateTime(billDetails.getDate()), new DateTime(new Date()))
//					.getDays() < ((billDetails.getFrequency() * 30) - 15)) {
//				ArrayList<String> tempList = userDTO.getServiceNotification();
//				tempList.add("cc");
//				userDTO.setServiceNotification(tempList);
//				break;
//			}
//
//		}
//		for (BillDetailsDth billDetails : user.getDthList()) {
//			if (Days.daysBetween(new DateTime(billDetails.getDate()), new DateTime(new Date()))
//					.getDays() < ((billDetails.getFrequency() * 30) - 15)) {
//				ArrayList<String> tempList = userDTO.getServiceNotification();
//				tempList.add("dth");
//				userDTO.setServiceNotification(tempList);
//				break;
//			}
//
//		}
//		for (BillDetailsElectricity billDetails : user.getElectricityList()) {
//			if (Days.daysBetween(new DateTime(billDetails.getDate()), new DateTime(new Date()))
//					.getDays() < ((billDetails.getFrequency() * 30) - 15)) {
//				ArrayList<String> tempList = userDTO.getServiceNotification();
//				tempList.add("electricity");
//				userDTO.setServiceNotification(tempList);
//				break;
//			}
//
//		}
//		for (BillDetailsInsurance billDetails : user.getInsuranceList()) {
//			if (Days.daysBetween(new DateTime(billDetails.getDate()), new DateTime(new Date()))
//					.getDays() < ((billDetails.getFrequency() * 30) - 15)) {
//				ArrayList<String> tempList = userDTO.getServiceNotification();
//				tempList.add("insurance");
//				userDTO.setServiceNotification(tempList);
//				break;
//			}
//
//		}
//		for (BillDetailsLoanAccount billDetails : user.getLoanAccountList()) {
//			if (Days.daysBetween(new DateTime(billDetails.getDate()), new DateTime(new Date()))
//					.getDays() < ((billDetails.getFrequency() * 30) - 15)) {
//				ArrayList<String> tempList = userDTO.getServiceNotification();
//				tempList.add("loan");
//				userDTO.setServiceNotification(tempList);
//				break;
//			}
//
//		}
//		for (BillDetailsTax billDetails : user.getTaxList()) {
//			if (Days.daysBetween(new DateTime(billDetails.getDate()), new DateTime(new Date()))
//					.getDays() < ((billDetails.getFrequency() * 30) - 15)) {
//				ArrayList<String> tempList = userDTO.getServiceNotification();
//				tempList.add("tax");
//				userDTO.setServiceNotification(tempList);
//				break;
//			}
//
//		}
//		for (BillDetailsTelephone billDetails : user.getTelephoneList()) {
//			if (Days.daysBetween(new DateTime(billDetails.getDate()), new DateTime(new Date()))
//					.getDays() < ((billDetails.getFrequency() * 30) - 15)) {
//				ArrayList<String> tempList = userDTO.getServiceNotification();
//				tempList.add("telephone");
//				userDTO.setServiceNotification(tempList);
//				break;
//			}
//
//		}
//		return userDTO;
//	}
	@Transactional
	public User getUserDetails(String userName) {
		return this.userRepository.getUser(userName);
	}
}
