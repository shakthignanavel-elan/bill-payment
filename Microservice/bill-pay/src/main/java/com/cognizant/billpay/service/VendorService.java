package com.cognizant.billpay.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.billpay.model.Vendor;
import com.cognizant.billpay.repository.VendorRepository;

@Service
public class VendorService {

	@Autowired
	VendorRepository vendorRepository;
	
	@Transactional
	public List<Vendor> getAllVendors() {
		return vendorRepository.findAll();
	}
	
	@Transactional
	public void updateVendorStatus(Vendor vendor) {
		vendorRepository.save(vendor);
	}
	
	@Transactional
	public List<Vendor> getVendor(String regNo) {
		return vendorRepository.findByVendorRegNo(regNo);
	}
	
	@Transactional
	public List<Vendor> getVendorOnType(String type) {
		return this.vendorRepository.findVendorOnType(type);
	}
	@Transactional
	public void updateVendor(Vendor vendor) {
		this.vendorRepository.save(vendor);
	}
}
