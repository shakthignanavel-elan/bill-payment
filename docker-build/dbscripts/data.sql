insert into billpay.role values (1,'ADMIN');
insert into billpay.role values (2,'USER');
insert into billpay.role values (3,'VENDOR');
insert into all_users values (1,'admin','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK');
insert into user_role values (1,1,1);
insert into vendor(ve_name,ve_company_register_number,ve_type,ve_address,ve_country,ve_state,ve_email,ve_contact_number,ve_website,ve_certificate_issued_date,ve_certificate_validity_date,ve_year_of_establish,ve_payment_gateway,ve_password,ve_image,ve_status,ve_remark)
values('TNEB','ELEC456','Electricity','No:800,Mount Road,Anna Salai,Chennai-02','India','TamilNadu','cemines@tnebnet.org',9445442300,'www.tneb.in','1950/06/17',
'2100/12/31',1950,'GPay','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK','https://upload.wikimedia.org/wikipedia/en/e/e9/Tamil_Nadu_Electricity_Board_%28emblem%29.jpg',0,'to be done'),
('LIC','LIC396','Insurance','No:70,Guindy,Chennai-03','India','TamilNadu','user@licnet.org',02268276827,'www.licindia.in','1987/05/07',
'2050/12/01',1987,'E-Wallet','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQO62XuJCP8NRoRpVGvYonfLjyBej8qo2Ae88JgfLFZM0WxucV7&s',0,'to be done'),
('Airtel','AIRTEL001','DTH','No:42,Santhome Road,Mylapore,Chennai-02','India','TamilNadu','myairtel@airnet.org',42027733,'www.myairtel.in','2000/06/17',
'2100/12/01',2008,'GPay','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK','https://upload.wikimedia.org/wikipedia/en/thumb/8/8d/Bharti_Airtel_Limited_logo.svg/361px-Bharti_Airtel_Limited_logo.svg.png',0,'to be done'),
('HDFC','HDFC0A1','Loan Account','No:759,ITC Centre,Anna Salai,Chennai-02','India','TamilNadu','customer@hdfcnet.org',07573919585,'www.tneb.in','1994/06/17',
'2070/12/31',1994,'E-Wallet','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRdIDk2rL0sq7cV73pxn6KkxEJYdGE878bXai1PjTpH9YqPCdFt&s',0,'to be done'),
('BSNL','BSNL006','Telephone','No:16,Greams Road,Chennai-006','India','TamilNadu','bsnl@bsnlnet.org',18003451504,'www.bsnl.co.in','2000/10/01',
'2100/12/31',2000,'GPay','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK','https://www.gizmotimes.com/wp-content/uploads/2016/04/BSNL-4G-serive-with-100Mbps-speed.jpg',0,'to be done'),
('MasterCard','CCREG002','Cerdit Card','No:805,Mount Road,Anna Salai,Chennai-02','India','TamilNadu','user@mastercard.org',8006227747,'www.mcard.com','1966/01/01',
'2100/12/01',1966,'E-Wallet','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR5jP7j7g8uIiHPQEQX4hbxHDO0QxidYDtNRSDg0uj4E1V0CrHa&s',0,'to be done'),
('TNIT','TNTAX001','Tax','No:121,M.G Road,Nungambakkam,Chennai-34','India','TamilNadu','tntax@ittaxnet.org',9445467553,'www.it.gov.in','1980/06/17',
'2100/01/01',1980,'E-Wallet','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ96qhl9rpKe6rdJTUcdAB5PrpFxOmBL4ICh-vIo-hR_EVGrfpj&s',0,'to be done');
insert into all_users(au_name,au_password)
values("TNEB",'$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK'),
("LIC",'$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK'),
("HDFC",'$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK'),
("Airtel",'$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK'),
("BSNL",'$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK'),
("MasterCard",'$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK'),
("TNIT",'$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK');
insert into user_role values(2,2,3);
insert into user_role values(3,3,3);
insert into user_role values(4,4,3);
insert into user_role values(5,5,3);
insert into user_role values(6,6,3);
insert into user_role values(7,7,3);
insert into user_role values(8,8,3);
insert into user values (1,"asd","asd",12,'male',1111111111,'aaaaaaaaa','ababab','first','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK');
insert into user values (2,"fgh","fgh",22,'female',22222222222,'bbbbbbb','cdcdcd','second','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK');
insert into user values (3,"jkl","jkl",32,'male',333333333333,'cccccccc','efefefe','third','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK');
insert into user values (4,"bnm","bnm",43,'female',44444444444,'dddddd','ghghgh','fourth','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK');
insert into bill_details_electricity values(1,1078,'2019/11/31','google pay','hello hi bye',1,1,3);
insert into bill_details_electricity values(2,1004,'2019/8/31','google pay','hello hi bye',2,1,3);
insert into bill_details_electricity values(3,1650,'2019/11/15','google pay','hello hi bye',2,1,3);
insert into bill_details_electricity values(4,1500,'2019/10/31','google pay','hello hi bye',2,1,3);
insert into all_users values(9,'first','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK');
insert into all_users values(10,'second','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK');
insert into all_users values(11,'third','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK');
insert into all_users values(12,'fourth','$2a$10$R/lZJuT9skteNmAku9Y7aeutxbOKstD5xE5bHOf74M2PHZipyt3yK');
insert into user_role values(9,9,2);
insert into user_role values(10,10,2);
insert into user_role values(11,11,2);
insert into user_role values(12,12,2);















