SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema billpay
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema billpay
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `billpay` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `billpay` ;

-- -----------------------------------------------------
-- Table `billpay`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `billpay`.`user` (
  `us_id` INT NOT NULL AUTO_INCREMENT,
  `us_first_name` TEXT(50) NULL,
  `us_last_name` TEXT(50) NULL,
  `us_age` INTEGER(2) NULL,
  `us_gender` VARCHAR(7) NULL, 
  `us_contact_number` BIGINT(10) NULL,
  `us_pan_number`  VARCHAR(10) NULL,
  `us_aadhar_number`  VARCHAR(10) NULL,
  `us_user_name`  VARCHAR(10) NULL,
  `us_password` VARCHAR(60) NULL,
 
  PRIMARY KEY (`us_id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `billpay`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `billpay`.`role` (
  `ro_id` INT NOT NULL AUTO_INCREMENT,
  `ro_name` VARCHAR(45) NULL,
  PRIMARY KEY (`ro_id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `billpay`.`user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `billpay`.`user_role` (
  `ur_id` INT NOT NULL AUTO_INCREMENT,
  `ur_au_id` INT NULL,
  `ur_ro_id` INT NULL,
  PRIMARY KEY (`ur_id`),
  INDEX `ur_au_fk_idx` (`ur_au_id` ASC),
  INDEX `ur_ro_fk_idx` (`ur_ro_id` ASC),
  CONSTRAINT `ur_au_fk`
    FOREIGN KEY (`ur_au_id`)
    REFERENCES `billpay`.`all_users` (`au_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ur_ro_fk`
    FOREIGN KEY (`ur_ro_id`)
    REFERENCES `billpay`.`role` (`ro_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `billpay`.`vendor` (
  `ve_id` INT NOT NULL AUTO_INCREMENT,
  `ve_name` VARCHAR(50) NULL,
  `ve_company_register_number` VARCHAR(50) NULL,
  `ve_type` VARCHAR(50) NULL,
  `ve_address` VARCHAR(200) NULL,
  `ve_country` VARCHAR(50) NULL,
  `ve_state` VARCHAR(20) NULL,
  `ve_email` VARCHAR(50) NULL,
  `ve_contact_number` BIGINT(12) NULL,
  `ve_website` VARCHAR(15) NULL,
  `ve_certificate_issued_date` DATE NULL,
  `ve_certificate_validity_date` DATE NULL,
  `ve_year_of_establish` INT NULL,
  `ve_payment_gateway` VARCHAR(15) NULL,
  `ve_password` VARCHAR(60) NULL,
  `ve_image` VARCHAR(500)NULL,
  `ve_status` INTEGER(2)NULL,
  `ve_remark` VARCHAR(200)NULL,
  PRIMARY KEY (`ve_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `billpay`.`all_users` (
  `au_id` INT NOT NULL AUTO_INCREMENT,
  `au_name` VARCHAR(45) NULL,
  `au_password` VARCHAR(60)NULL,
   PRIMARY KEY (`au_id`))
ENGINE = InnoDB; 

CREATE TABLE IF NOT EXISTS `billpay`.`bill_details_electricity`(
  `bd_el_id` INT NOT NULL AUTO_INCREMENT,
  `bd_el_amount` BIGINT(15) NULL,
  `bd_el_date` DATE NULL,
  `bd_el_payment_type` VARCHAR(15) NULL,
  `bd_el_account_number` VARCHAR(45) NULL, 
  `bd_el_frequency` INT NULL,
   `bd_el_us_id` INT NULL,
  `bd_el_ve_id` INT NULL,
   PRIMARY KEY (`bd_el_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `billpay`.`bill_details_dth`(
  `bd_dt_id` INT NOT NULL AUTO_INCREMENT,
  `bd_dt_amount` BIGINT(15) NULL,
  `bd_dt_date` DATE NULL,
  `bd_dt_payment_type` VARCHAR(15) NULL,
  `bd_dt_customer_id` VARCHAR(40) NULL,
  `bd_dt_frequency` INT NULL,
   `bd_dt_us_id` INT NULL,
  `bd_dt_ve_id` INT NULL,
   PRIMARY KEY (`bd_dt_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `billpay`.`bill_details_telephone`(
  `bd_te_id` INT NOT NULL AUTO_INCREMENT,
  `bd_te_amount` BIGINT(15) NULL,
  `bd_te_date` DATE NULL,
  `bd_te_payment_type` VARCHAR(15) NULL,
  `bd_te_number` BIGINT NULL,
  `bd_te_frequency` INT NULL,
   `bd_te_us_id` INT NULL,
  `bd_te_ve_id` INT NULL,
   PRIMARY KEY (`bd_te_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `billpay`.`bill_details_tax`(
  `bd_ta_id` INT NOT NULL AUTO_INCREMENT,
  `bd_ta_amount` BIGINT(15) NULL,
  `bd_ta_date` DATE NULL,
  `bd_ta_payment_type` VARCHAR(15) NULL,
  `bd_ta_pan_number` VARCHAR(20) NULL,
  `bd_ta_frequency` INT NULL,
   `bd_ta_us_id` INT NULL,
  `bd_ta_ve_id` INT NULL,
  PRIMARY KEY (`bd_ta_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `billpay`.`bill_details_insurance`(
  `bd_in_id` INT NOT NULL AUTO_INCREMENT,
  `bd_in_amount` BIGINT(15) NULL,
  `bd_in_date` DATE NULL,
  `bd_in_payment_type` VARCHAR(15) NULL,
  `bd_in_policy_number` BIGINT NULL,
  `bd_in_date_of_birth` DATE NULL,
  `bd_in_account_name` VARCHAR(20) NULL,
  `bd_in_frequency` INT NULL,
   `bd_in_us_id` INT NULL,
  `bd_in_ve_id` INT NULL,
  PRIMARY KEY (`bd_in_id`))

ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `billpay`.`bill_details_credit_card`(
  `bd_cc_id` INT NOT NULL AUTO_INCREMENT,
  `bd_cc_amount` BIGINT(15) NULL,
  `bd_cc_date` DATE NULL,
  `bd_cc_payment_type` VARCHAR(15) NULL,
  `bd_cc_card_number` BIGINT NULL, 
  `bd_cc_frequency` INT NULL,
   `bd_cc_us_id` INT NULL,
  `bd_cc_ve_id` INT NULL,
  PRIMARY KEY (`bd_cc_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `billpay`.`bill_details_loan_account`(
  `bd_la_id` INT NOT NULL AUTO_INCREMENT,
  `bd_la_amount` BIGINT(15) NULL,
  `bd_la_date` DATE NULL,
  `bd_la_payment_type` VARCHAR(15) NULL,
  `bd_la_account_number` BIGINT NULL,
  `bd_la_account_name` VARCHAR(20) NULL,
  `bd_la_frequency` INT NULL,
  `bd_la_us_id` INT NULL,
  `bd_la_ve_id` INT NULL,
   PRIMARY KEY (`bd_la_id`))
  
ENGINE = InnoDB;

 
