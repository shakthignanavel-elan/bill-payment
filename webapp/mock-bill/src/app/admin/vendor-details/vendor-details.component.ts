import { Component, OnInit } from '@angular/core';
import { Vendor } from 'src/app/site/vendor';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EndpointService } from 'src/app/endpoint.service';
import { Router } from '@angular/router';
import { element } from 'protractor';
import { VendorService } from '../vendor.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-vendor-details',
  templateUrl: './vendor-details.component.html',
  styleUrls: ['./vendor-details.component.css']
})
export class VendorDetailsComponent implements OnInit {

  vendorItems: Vendor[];
  editForm: FormGroup;
  countries: any;
  countriesMap: Map<Number, String>;
  numbers: number[];
  countryResponse: boolean = false;
  buttonClicked: boolean = false;
  dataLoaded: Promise<boolean>;
  countryLoaded: Promise<boolean>;
  vendor: Vendor = {
    vendorId: 0,
    remark: '',
    imageURL: "",
    address: "",
    contactNumber: "",
    country: "",
    email: "",
    establishedYear: 0,
    gateway: "",
    issuedDate: new Date(),
    password: "",
    state: "",
    validityDate: new Date(),
    vendorName: "",
    vendorRegNo: "",
    vendorType: "",
    website: "",
    status: 0

  }
  constructor(private endpointService: EndpointService, private router: Router,private vendorService:VendorService,private datePipe:DatePipe) {
  }

  ngOnInit() {
    // this.vendor.address = "qweqweqweqweqwe";
    // this.vendor.contactNumber = "12345678912";
    // this.vendor.country = "qwerty";
    // this.vendor.state = "ytrewq";
    // this.vendor.vendorName = "hello";
    // this.vendor.vendorRegNo = "1";
    // this.vendor.website = "qweqweqwe";
    // this.vendor.email = "asd@asd.com";
    // this.vendor.vendorType = "Electricity";
    // this.vendor.establishedYear = 1100;
    // this.vendor.imageURL = "https://www.google.co.in/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png";
    // this.vendor.status = 1;
    // this.vendor.vendorId = 1;
    // this.vendor.remark = "hello check your userName";
    // this.vendorItems = [this.vendor, this.vendor];
    this.vendorService.getVendorList().subscribe((response:any)=>{
      this.vendorItems=response;
      this.dataLoaded=Promise.resolve(true);
    })
    this.editForm = new FormGroup({});
    this.countriesMap = new Map();
    this.editForm.addControl('vendorName', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.editForm.addControl('vendorRegNo', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.editForm.addControl('addrLine', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.editForm.addControl('imageURL', new FormControl("", [Validators.required, Validators.maxLength(500), Validators.minLength(2)]));
    this.editForm.addControl('contactNum', new FormControl("", [Validators.required, Validators.maxLength(12), Validators.minLength(12), Validators.pattern("^[0-9]*$")]));
    this.editForm.addControl('country', new FormControl("", [Validators.required]));
    this.editForm.addControl('email', new FormControl("", [Validators.email, Validators.required]));
    this.editForm.addControl('website', new FormControl("", [Validators.required, Validators.maxLength(15), Validators.minLength(2)]));
    this.editForm.addControl('issuedDate', new FormControl("", [Validators.required]));
    this.editForm.addControl('validityDate', new FormControl("", [Validators.required]));
    this.editForm.addControl('establishedYr', new FormControl("", [Validators.required]));
    this.editForm.addControl('vendorType', new FormControl("", [Validators.required]));
    this.editForm.addControl('gateway', new FormControl("", [Validators.required, Validators.maxLength(15), Validators.minLength(2)]));
    this.editForm.addControl('state', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.getCountry();
    let date: number = new Date().getFullYear() - 999;
    this.numbers = Array(1000).fill(0).map((x, i) => i + date);
  }
  edit(vendorId: number) {
    this.vendorItems.forEach((vendorItem) => {
      if (vendorItem.vendorId == vendorId) {
        this.vendor = vendorItem;
        console.log(this.vendor)
        this.editForm.controls.vendorName.setValue(vendorItem.vendorName);
        this.editForm.controls.vendorRegNo.setValue(vendorItem.vendorRegNo);
        this.editForm.controls.addrLine.setValue(vendorItem.address);
        this.editForm.controls.imageURL.setValue(vendorItem.imageURL);
        this.editForm.controls.contactNum.setValue(vendorItem.contactNumber);
        this.editForm.controls.email.setValue(vendorItem.email);
        this.editForm.controls.website.setValue(vendorItem.website);
        this.editForm.controls.country.setValue(this.vendor.country);
        this.editForm.controls.issuedDate.setValue(vendorItem.issuedDate);
        this.editForm.controls.validityDate.setValue(vendorItem.validityDate);
        this.editForm.controls.establishedYr.setValue(vendorItem.establishedYear);
        this.editForm.controls.vendorType.setValue(vendorItem.vendorType);
        this.editForm.controls.gateway.setValue(vendorItem.gateway);
        this.editForm.controls.state.setValue(vendorItem.state);
      }
    });
    this.buttonClicked = true;
  }
  getCountry() {
    const endpointCountry = this.endpointService.countryEndpoint().subscribe((response) => {
      this.countriesMap = response['result'];
      this.countryResponse = true;
      this.countryLoaded=Promise.resolve(true);
      this.countries = response;
      console.log(response);
      //return response;
    },(errors)=>{
      console.log(errors);
    });
    if (this.countryResponse == true) {
      endpointCountry.unsubscribe();
    }
  }

  sendValue() {
    this.vendor.vendorName = this.editForm.value.vendorName;
    this.vendor.address = this.editForm.value.addrLine;
    this.vendor.imageURL = this.editForm.value.imageURL;
    this.vendor.contactNumber = this.editForm.value.contactNum;
    this.vendor.country = this.editForm.value.country;
    this.vendor.email = this.editForm.value.email;
    this.vendor.website = this.editForm.value.website;
    this.vendor.issuedDate = this.editForm.value.issuedDate;
    this.vendor.validityDate = this.editForm.value.validityDate;
    this.vendor.establishedYear = this.editForm.value.establishedYr;
    this.vendor.vendorType = this.editForm.value.vendorType;
    this.vendor.gateway = this.editForm.value.gateway;
    this.vendor.state = this.editForm.value.state;
    this.vendor.status = 0;
    this.endpointService.editVendor(this.vendor).subscribe((response)=>{
      console.log("success");
    })
    this.vendor = null;
  }
}
