import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Vendor } from 'src/app/site/vendor';

@Component({
  selector: 'app-vendor-item',
  templateUrl: './vendor-item.component.html',
  styleUrls: ['./vendor-item.component.css']
})
export class VendorItemComponent implements OnInit {

  @Input() vendor: Vendor;
  @Output() viewDetailsEmitter = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  review(id: number) {
    this.viewDetailsEmitter.emit(id);
  }
}
