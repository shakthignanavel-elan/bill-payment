import { Injectable, OnInit } from '@angular/core';
import { Vendor } from '../site/vendor';
import { EndpointService } from '../endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class VendorService implements OnInit {
  vendorList:Vendor[];
  ngOnInit(): void {
  }

  // vendor: Vendor = {
  //   vendorId: 0,
  //   imageURL: "",
  //   address: "",
  //   contactNumber: "",
  //   country: "",
  //   email: "",
  //   establishedYear: 0,
  //   gateway: "",
  //   issuedDate: new Date(),
  //   password: "",
  //   state: "",
  //   validityDate: new Date(),
  //   vendorName: "",
  //   vendorRegNo: "",
  //   vendorType: "",
  //   website: "",
  //   status: 0,
  //   remark: ""

  // }
  constructor(private endpointService: EndpointService) { 
    // this.vendor.imageURL = "https://www.google.co.in/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png",
    //   this.vendor.address = "dubai,dubai main,dubai",
    //   this.vendor.contactNumber = "12345678912",
    //   this.vendor.country = "India",
    //   this.vendor.email = "12@12.com",
    //   this.vendor.establishedYear = 1500,
    //   this.vendor.gateway = "pay",
    //   this.vendor.issuedDate = new Date(),
    //   this.vendor.password = "pwd",
    //   this.vendor.state = "assam",
    //   this.vendor.validityDate = new Date(),
    //   this.vendor.vendorName = "helloi",
    //   this.vendor.vendorRegNo = "123sd123",
    //   this.vendor.vendorType = "electricity",
    //   this.vendor.website = "www.www.com",
    //   this.vendor.status = 1
    //   this.vendor.vendorId=2
  }
  getAllVendors() {
    return this.endpointService.getAllVendors();
  }
  updateVendorStatus(vendor: Vendor) {
      return this.endpointService.updateVendorStatus(vendor);
  }
  getVendorList() {
    return this.endpointService.getVendorList();
  }
  editVendor(vendor:Vendor) {
    return this.endpointService.editVendor(vendor);
  }
  getVendorListBasedOnType(type:string) {
    return this.endpointService.getVendorListBasedOnType(type);
  }
}
