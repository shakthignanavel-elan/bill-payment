import { Component, OnInit, Input, Output } from '@angular/core';
import { Vendor } from '../../site/vendor'
import { EventEmitter } from 'events';
import { VendorService } from '../vendor.service';
import { element } from 'protractor';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit {

  vendorItems: Vendor[];
  buttonClicked: boolean=false;
  vendor: Vendor;
  date: string;
  reviewForm: FormGroup;
  waitForCall: Observable<boolean>;
  DataLoaded: Promise<boolean>;

  
  /*vendor: Vendor = {
    imageURL: "",
    address: "",
    contactNumber: "",
    country: "",
    email: "",
    establishedYear: 0,
    gateway: "",
    issuedDate: new Date(),
    password: "",
    state: "",
    validityDate: new Date(),
    vendorName: "",
    vendorRegNo: "",
    vendorType: "",
    website: "",
    status: 0

  }*/
  constructor(private vendorService: VendorService) { }

  ngOnInit() {
    this.reviewForm=new FormGroup({});
    this.reviewForm.addControl('reveiewTextBox',new FormControl("",[Validators.required,Validators.maxLength(200),Validators.minLength(2)]))
    this.getAllVendors();
    // this.vendor.address = "qweqweqweqweqwe";
    // this.vendor.contactNumber = "12345678912";
    // this.vendor.country = "qwerty";
    // this.vendor.state = "ytrewq";
    // this.vendor.vendorName = "hello";
    // this.vendor.vendorRegNo = "1";
    // this.vendor.website = "qweqweqwe";
    // this.vendor.email = "asd@asd.com";
    // this.vendor.establishedYear = 1100;
    // this.vendor.imageURL = "https://www.google.co.in/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png";
    // this.vendor.status = 1;
  }

  reviewVendor(vendorId) {
    this.buttonClicked=true;
    this.DataLoaded = Promise.resolve(false);
    this.vendorItems.forEach(element=> {
      if(element.vendorId==vendorId) {
        this.vendor=element;
        stop;
      }
    });
    
  }

  getAllVendors() {
    this.vendorService.getAllVendors().subscribe((response:any)=>{
      this.vendorItems=response;
    });
  }
  acceptVendor() {
    let tempStatus=this.vendor.status;
    let newVendor: Vendor=this.vendor;
    newVendor.status=1;
    let acknowledge=true;
    this.vendorService.updateVendorStatus(newVendor).subscribe((response)=>{
      this.DataLoaded = Promise.resolve(true);
      this.vendor.status=1;
      return true;
    },(errors)=>{
      this.vendor.status=tempStatus;
      return false;
    })
  }
  rejectVendor() {
    let tempStatus=this.vendor.status;
    let newVendor: Vendor=this.vendor;
    newVendor.status=2;
    let acknowledge=true;
    this.vendorService.updateVendorStatus(newVendor).subscribe((response)=>{
      this.DataLoaded = Promise.resolve(true);
      this.vendor.status=2;
      return true;
    },(errors)=>{
      this.vendor.status=tempStatus;
      return false;
    })
  }
  sendReview() {
    let tempStatus=this.vendor.status;
    let newVendor: Vendor=this.vendor;
    newVendor.status=3;
    newVendor.remark=this.reviewForm.value.reveiewTextBox;
    let acknowledge=true;
    this.vendorService.updateVendorStatus(newVendor).subscribe((response)=>{
      this.DataLoaded = Promise.resolve(true);
      this.vendor.status=3;
      return true;
    },(errors)=>{
      this.vendor.status=tempStatus;
      return false;
    })
  }
  isHidden() {
    this.waitForCall.subscribe()
  }

}
