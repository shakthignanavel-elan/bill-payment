import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './site/login/login.component';
import { SignupComponent } from './site/signup/signup.component';
import { VendorRegistrationComponent } from './site/vendor-registration/vendor-registration.component';
import { UserRegistrationComponent } from './site/user-registration/user-registration.component';
import { VendorComponent } from './admin/vendor/vendor.component';
import { AuthguardGuard } from './site/authguard.guard';
import { HomepageComponent } from './site/homepage/homepage.component';
import { ElectricityComponent } from './bill-payment/electricity/electricity.component';
import { DthComponent } from './bill-payment/dth/dth.component';
import { TelephoneComponent } from './bill-payment/telephone/telephone.component';
import { TaxComponent } from './bill-payment/tax/tax.component';
import { InsuranceComponent } from './bill-payment/insurance/insurance.component';
import { CreditCardComponent } from './bill-payment/credit-card/credit-card.component';
import { LoanComponent } from './bill-payment/loan/loan.component';
import { MenuComponent } from './user/menu/menu.component';
import { VendorDetailsComponent } from './admin/vendor-details/vendor-details.component';
import { GatewayOptionsComponent } from './bill-payment/gateway-options/gateway-options.component';
import { PaymentComponent } from './bill-payment/payment/payment.component';


const routes: Routes = [{path:"login",component: LoginComponent},
{path:"",component:HomepageComponent},
{path:"user-signup",component:UserRegistrationComponent},
{path:"vendor-signup",component:VendorRegistrationComponent},
{path:"registration",component:SignupComponent},
{path:"vendor-list",component: VendorComponent,canActivate: [AuthguardGuard]},
{path:"electricity-payment",component:ElectricityComponent,canActivate:[AuthguardGuard]},
{path:"dth-payment",component:DthComponent,canActivate:[AuthguardGuard]},
{path:"telephone-payment",component:TelephoneComponent,canActivate:[AuthguardGuard]},
{path:"tax-payment",component:TaxComponent,canActivate:[AuthguardGuard]},
{path:"insurance-payment",component:InsuranceComponent,canActivate:[AuthguardGuard]},
{path:"creditcard-payment",component:CreditCardComponent,canActivate:[AuthguardGuard]},
{path:"loan-payment",component:LoanComponent,canActivate:[AuthguardGuard]},
{path:"user-menu",component:MenuComponent,canActivate:[AuthguardGuard]},
{path:"vendor-details",component:VendorDetailsComponent,canActivate:[AuthguardGuard]},
{path:"gateway-options",component:GatewayOptionsComponent,canActivate:[AuthguardGuard]},
{path:"payment",component:PaymentComponent,canActivate:[AuthguardGuard]}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
