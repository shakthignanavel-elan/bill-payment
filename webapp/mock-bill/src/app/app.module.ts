import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './site/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './site/header/header.component';
import { SignupComponent } from './site/signup/signup.component';
import { VendorRegistrationComponent } from './site/vendor-registration/vendor-registration.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatCardModule,
  MatRadioModule,
  MatDatepickerModule,
  NativeDateModule,
  MatNativeDateModule
} from '@angular/material';
import { MenuComponent } from './user/menu/menu.component';
import { VendorComponent } from './admin/vendor/vendor.component';
import { VendorItemComponent } from './admin/vendor-item/vendor-item.component';
import { VendorDetailsComponent } from './admin/vendor-details/vendor-details.component';
import { UserRegistrationComponent } from './site/user-registration/user-registration.component';
import { HomepageComponent } from './site/homepage/homepage.component';
import { ElectricityComponent } from './bill-payment/electricity/electricity.component';
import { DthComponent } from './bill-payment/dth/dth.component';
import { TelephoneComponent } from './bill-payment/telephone/telephone.component';
import { TaxComponent } from './bill-payment/tax/tax.component';
import { InsuranceComponent } from './bill-payment/insurance/insurance.component';
import { CreditCardComponent } from './bill-payment/credit-card/credit-card.component';
import { LoanComponent } from './bill-payment/loan/loan.component';
import { DatePipe } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { GatewayOptionsComponent } from './bill-payment/gateway-options/gateway-options.component';
import { PaymentComponent } from './bill-payment/payment/payment.component';
import { BillHistoryComponent } from './user/bill-history/bill-history.component';
import { BillComponent } from './user/bill/bill.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    SignupComponent,
    VendorRegistrationComponent,
    MenuComponent,
    VendorComponent,
    VendorItemComponent,
    VendorDetailsComponent,
    UserRegistrationComponent,
    HomepageComponent,
    ElectricityComponent,
    DthComponent,
    TelephoneComponent,
    TaxComponent,
    InsuranceComponent,
    CreditCardComponent,
    LoanComponent,
    GatewayOptionsComponent,
    PaymentComponent,
    BillHistoryComponent,
    BillComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatRadioModule,
    MatDatepickerModule,
    NativeDateModule,
    MatNativeDateModule,
    BsDatepickerModule.forRoot()     // <----- import for date formating(optional)
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
