import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControlName, FormControl, Validators } from '@angular/forms';
import { Vendor } from 'src/app/site/vendor';
import { PaymentService } from '../payment.service';
import { VendorService } from 'src/app/admin/vendor.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.css']
})
export class CreditCardComponent implements OnInit {

  paymentForm: FormGroup;
  accountNumber: FormControlName;
  amount: FormControlName;
  frequency: FormControlName;
  vendor: FormControlName;
  vendors: Vendor[];
  registerStatus: boolean = true;
  creditCardValue:any={};
  userExists: boolean = false;
  vendor1: Vendor = {
    vendorId: 0,
    imageURL: "",
    address: "",
    contactNumber: "",
    country: "",
    email: "",
    establishedYear: 0,
    gateway: "",
    issuedDate: new Date(),
    password: "",
    state: "",
    validityDate: new Date(),
    vendorName: "",
    vendorRegNo: "",
    vendorType: "",
    website: "",
    status: 0,
    remark: ""

  }
  constructor(private router:Router,private paymentService:PaymentService,private vendorService:VendorService) {
    this.paymentForm = new FormGroup({});
    this.paymentForm.addControl('cardNumber', new FormControl("", [Validators.required, Validators.maxLength(45), Validators.pattern("^[0-9]*$")]));
    this.paymentForm.addControl('amount', new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]));
    this.paymentForm.addControl('frequency', new FormControl('', [Validators.required]));
    this.paymentForm.addControl('vendor', new FormControl('', [Validators.required]));
  }

  ngOnInit() {
    this.vendorService.getVendorListBasedOnType("Credit Card").subscribe((response:any)=>{
      this.vendors=response;
    })
  }

  sendValue() {
    this.creditCardValue['amount']=this.paymentForm.value.amount;
    this.creditCardValue['frequency']=this.paymentForm.value.frequency;
    this.vendors.forEach((element)=> {
      if(this.paymentForm.value.vendor==element.vendorName) {
        this.creditCardValue['vendor']=element;
      }
    });
    this.creditCardValue['cardNumber']=this.paymentForm.value.cardNumber;
    this.paymentService.setformData(this.creditCardValue);
    this.paymentService.formName="Credit Card";
    this.router.navigate(["/gateway-options"]);
  }
}
