import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControlName, FormControl, Validators } from '@angular/forms';
import { Vendor } from 'src/app/site/vendor';
import { VendorService } from 'src/app/admin/vendor.service';
import { PaymentService } from '../payment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dth',
  templateUrl: './dth.component.html',
  styleUrls: ['./dth.component.css']
})
export class DthComponent implements OnInit {

  paymentForm: FormGroup;
  accountNumber: FormControlName;
  amount: FormControlName;
  frequency: FormControlName;
  vendor: FormControlName;
  vendors: Vendor[];
  dthValue:any={};
  registerStatus: boolean = true;
  userExists: boolean = false;
  vendor1: Vendor = {
    vendorId: 0,
    imageURL: "",
    address: "",
    contactNumber: "",
    country: "",
    email: "",
    establishedYear: 0,
    gateway: "",
    issuedDate: new Date(),
    password: "",
    state: "",
    validityDate: new Date(),
    vendorName: "",
    vendorRegNo: "",
    vendorType: "",
    website: "",
    status: 0,
    remark: ""

  }
  constructor(private router:Router,private vendorService: VendorService,private paymentService: PaymentService) {
    this.paymentForm = new FormGroup({});
    this.paymentForm.addControl('customerId', new FormControl("", [Validators.required, Validators.maxLength(45)]));
    this.paymentForm.addControl('amount', new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]));
    this.paymentForm.addControl('frequency', new FormControl('', [Validators.required]));
    this.paymentForm.addControl('vendor', new FormControl('', [Validators.required]));
  }

  ngOnInit() {
    this.vendorService.getVendorListBasedOnType("DTH").subscribe((response:any)=>{
      this.vendors=response;
    })
  }

  sendValue() {
    this.dthValue['amount']=this.paymentForm.value.amount;
    this.dthValue['frequency']=this.paymentForm.value.frequency;
    this.vendors.forEach((element)=> {
      if(this.paymentForm.value.vendor==element.vendorName) {
        this.dthValue['vendor']=element;
      }
    });
    this.dthValue['customerID']=this.paymentForm.value.customerId;
    this.paymentService.setformData(this.dthValue);
    this.paymentService.formName="DTH";
    this.router.navigate(["/gateway-options"]);
  }

}
