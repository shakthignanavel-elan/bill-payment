import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayOptionsComponent } from './gateway-options.component';

describe('GatewayOptionsComponent', () => {
  let component: GatewayOptionsComponent;
  let fixture: ComponentFixture<GatewayOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatewayOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
