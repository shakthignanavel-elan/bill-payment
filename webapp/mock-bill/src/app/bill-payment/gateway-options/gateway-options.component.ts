import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PaymentService } from '../payment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gateway-options',
  templateUrl: './gateway-options.component.html',
  styleUrls: ['./gateway-options.component.css']
})
export class GatewayOptionsComponent implements OnInit {

  paymentGroup: FormGroup;
  constructor(private paymentService:PaymentService,private router:Router) { }

  ngOnInit() {
    this.paymentGroup = new FormGroup({});
    this.paymentGroup.addControl('paymentMethod',new FormControl('',[Validators.required]));
  }
  sendValue() {
    this.paymentService.paymentMethod=this.paymentGroup.value.paymentMethod;
    this.router.navigate(['payment']);
  }

}
