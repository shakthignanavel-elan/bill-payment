import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControlName, FormControl, Validators } from '@angular/forms';
import { Vendor } from 'src/app/site/vendor';
import { VendorService } from 'src/app/admin/vendor.service';
import { PaymentService } from '../payment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-insurance',
  templateUrl: './insurance.component.html',
  styleUrls: ['./insurance.component.css']
})
export class InsuranceComponent implements OnInit {

  paymentForm: FormGroup;
  vendors: Vendor[];
  registerStatus: boolean = true;
  userExists: boolean = false;
  insuranceValue:any={};
  vendor1: Vendor = {
    vendorId: 0,
    imageURL: "",
    address: "",
    contactNumber: "",
    country: "",
    email: "",
    establishedYear: 0,
    gateway: "",
    issuedDate: new Date(),
    password: "",
    state: "",
    validityDate: new Date(),
    vendorName: "",
    vendorRegNo: "",
    vendorType: "",
    website: "",
    status: 0,
    remark: ""

  }
  constructor(private router:Router,private vendorService: VendorService,private paymentService:PaymentService) {
    this.paymentForm = new FormGroup({});
    this.paymentForm.addControl('policyNumber', new FormControl("", [Validators.required, Validators.maxLength(12),Validators.minLength(12)]));
    this.paymentForm.addControl('accName',new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(20)]))
    this.paymentForm.addControl('dateOfBirth',new FormControl('',[Validators.required]));
    this.paymentForm.addControl('amount', new FormControl('', [Validators.required]));
    this.paymentForm.addControl('frequency', new FormControl('', [Validators.required]));
    this.paymentForm.addControl('vendor', new FormControl('', [Validators.required]));
  }

  ngOnInit() {
    this.vendorService.getVendorListBasedOnType("Insurance").subscribe((response:any)=>{
      this.vendors=response;
    })
  }

  sendValue() {
    this.insuranceValue['amount']=this.paymentForm.value.amount;
    this.insuranceValue['frequency']=this.paymentForm.value.frequency;
    this.vendors.forEach((element)=> {
      if(this.paymentForm.value.vendor==element.vendorName) {
        this.insuranceValue['vendor']=element;
      }
    });
    this.insuranceValue['policyNumber']=this.paymentForm.value.policyNumber;
    this.insuranceValue['accountName']=this.paymentForm.value.accName;
    this.insuranceValue['dateOfBirth']=this.paymentForm.value.dateOfBirth;
    this.paymentService.setformData(this.insuranceValue);
    console.log(this.insuranceValue);
    this.paymentService.formName="Insurance"
    this.router.navigate(["/gateway-options"]);
  }

}
