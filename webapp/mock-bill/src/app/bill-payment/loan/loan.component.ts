import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControlName, FormControl, Validators } from '@angular/forms';
import { Vendor } from 'src/app/site/vendor';
import { VendorService } from 'src/app/admin/vendor.service';
import { PaymentService } from '../payment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css']
})
export class LoanComponent implements OnInit {
  paymentForm: FormGroup;
  accountNumber: FormControlName;
  amount: FormControlName;
  frequency: FormControlName;
  vendor: FormControlName;
  vendors: Vendor[];
  registerStatus: boolean = true;
  userExists: boolean = false;
  loanValue: any={};
  vendor1: Vendor = {
    vendorId: 0,
    imageURL: "",
    address: "",
    contactNumber: "",
    country: "",
    email: "",
    establishedYear: 0,
    gateway: "",
    issuedDate: new Date(),
    password: "",
    state: "",
    validityDate: new Date(),
    vendorName: "",
    vendorRegNo: "",
    vendorType: "",
    website: "",
    status: 0,
    remark: ""

  }
  constructor(private router: Router,private vendorService: VendorService,private paymentService:PaymentService) {
    this.paymentForm = new FormGroup({});
    this.paymentForm.addControl('loanAccNumber', new FormControl("", [Validators.required, Validators.maxLength(45), Validators.pattern("^[0-9]*$")]));
    this.paymentForm.addControl('amount', new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")  ]));
    this.paymentForm.addControl('frequency', new FormControl('', [Validators.required]));
    this.paymentForm.addControl('vendor', new FormControl('', [Validators.required]));
    this.paymentForm.addControl('accountName',new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(20)]));
    // this.vendor1.imageURL = "https://www.google.co.in/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png";
    // this.vendor1.address = "dubai,dubai main,dubai";
    // this.vendor1.contactNumber = "12345678912";
    // this.vendor1.country = "India";
    // this.vendor1.email = "12@12.com";
    // this.vendor1.establishedYear = 1500;
    // this.vendor1.gateway = "pay";
    // this.vendor1.issuedDate = new Date();
    // this.vendor1.password = "pwd";
    // this.vendor1.state = "assam";
    // this.vendor1.validityDate = new Date();
    // this.vendor1.vendorName = "helloi";
    // this.vendor1.vendorRegNo = "123sd123";
    // this.vendor1.vendorType = "electricity";
    // this.vendor1.website = "www.www.com";
    // this.vendor1.status = 1;
    // this.vendor1.vendorId = 2;
    // this.vendors = [this.vendor1, this.vendor1];
  }

  ngOnInit() {
    this.vendorService.getVendorListBasedOnType("Loan").subscribe((response:any)=>{
      this.vendors=response;
    })
  }

  sendValue() {
    this.loanValue['amount']=this.paymentForm.value.amount;
    this.loanValue['frequency']=this.paymentForm.value.frequency;
    this.vendors.forEach((element)=> {
      if(this.paymentForm.value.vendor==element.vendorName) {
        this.loanValue['vendor']=element;
      }
    });
    this.loanValue['accountNumber']=this.paymentForm.value.loanAccNumber;
    this.loanValue['accountName']=this.paymentForm.value.accName;
    this.paymentService.setformData(this.loanValue);
    this.paymentService.formName="Loan";
    console.log(this.loanValue);
    this.router.navigate(["/gateway-options"]);
  }

}
