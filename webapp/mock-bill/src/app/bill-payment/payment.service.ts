import { Injectable } from '@angular/core';
import { EndpointService } from '../endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  formName:string="";
  formData:any;
  paymentMethod:String;
  userDetails:any;
  constructor(private endpointService: EndpointService) { }
  setformData(object:any) {
    this.formData=object;
  }
  getFormData() {
    return this.formData;
  }
  setPaymentMethod(paymentType:string) {
    this.formData['paymentType']=paymentType;
  }
  sendData(formData:any) {
    switch(this.formName) {
      case "Credit Card":return this.endpointService.saveBill(formData,1);
      case "DTH":return this.endpointService.saveBill(formData,2);
      case "Electricity":return this.endpointService.saveBill(formData,3);
      case "Insurance":return this.endpointService.saveBill(formData,4);
      case "Loan":return this.endpointService.saveBill(formData,5);
      case "Tax":return this.endpointService.saveBill(formData,6);
      case "Telephone":return this.endpointService.saveBill(formData,7);
    }
  }
}
