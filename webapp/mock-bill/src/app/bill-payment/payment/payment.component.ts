import { Component, OnInit } from '@angular/core';
import { PaymentService } from '../payment.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  paymentType: number;
  paymentGateway: string = "";
  paymentForm: FormGroup;
  paymentData: any = {};
  constructor(private paymentService: PaymentService,private router: Router) { }

  ngOnInit() {
    this.paymentForm = new FormGroup({});
    switch (this.paymentService.paymentMethod) {
      case "cc":
        this.paymentType = 1;
        this.paymentGateway = "Credit Card"
        this.paymentForm.addControl('ccNumber', new FormControl('', [Validators.required, Validators.maxLength(15), Validators.minLength(3)]));
        this.paymentForm.addControl('cvv', new FormControl('', [Validators.required, Validators.maxLength(3), Validators.minLength(3), Validators.pattern("^[0-9]*$")]));
        break;
      case "netbanking":
        this.paymentGateway = "Net Banking";
        this.paymentForm.addControl('userId', new FormControl('', [Validators.required, Validators.maxLength(14), Validators.minLength(3)]));
        this.paymentForm.addControl('password', new FormControl('', [Validators.required, Validators.maxLength(14), Validators.minLength(3)]));
        this.paymentType = 2;
        break;
      case "paytm":
        this.paymentGateway = "Paytm";
        this.paymentForm.addControl('mobileNumber', new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern("^[0-9]*$")]));
        this.paymentForm.addControl('password', new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(13)]));
        this.paymentType = 3;
        break;
      case "googlePay":
        this.paymentGateway = "Google Pay"
        this.paymentForm.addControl('mobileNumber', new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]));
        this.paymentForm.addControl('mpin', new FormControl('', [Validators.required, Validators.maxLength(4), Validators.minLength(4), Validators.pattern("^[0-9]*$")]));
        this.paymentType = 4;
        break;
    }
    this.paymentData=this.paymentService.formData;
  }
  sendValue() {

    this.paymentData['date'] = new Date();
    this.paymentData['paymentType'] = this.paymentGateway;
    this.paymentData['user']=this.paymentService.userDetails;
    console.log("payment form data");
    console.log(this.paymentData);
    this.paymentService.sendData(this.paymentData).subscribe((response)=>{
      console.log("response");
      console.log(response);
      this.router.navigate(['user-menu']);
    })
    console.log(this.paymentForm.value);
  }

}
