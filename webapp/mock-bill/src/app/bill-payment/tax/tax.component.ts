import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControlName, FormControl, Validators } from '@angular/forms';
import { Vendor } from 'src/app/site/vendor';
import { VendorService } from 'src/app/admin/vendor.service';
import { PaymentService } from '../payment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['./tax.component.css']
})
export class TaxComponent implements OnInit {

  paymentForm: FormGroup;
  accountNumber: FormControlName;
  amount: FormControlName;
  frequency: FormControlName;
  vendor: FormControlName;
  vendors: Vendor[];
  registerStatus: boolean = true;
  userExists: boolean = false;
  taxValue:any={};
  vendor1: Vendor = {
    vendorId: 0,
    imageURL: "",
    address: "",
    contactNumber: "",
    country: "",
    email: "",
    establishedYear: 0,
    gateway: "",
    issuedDate: new Date(),
    password: "",
    state: "",
    validityDate: new Date(),
    vendorName: "",
    vendorRegNo: "",
    vendorType: "",
    website: "",
    status: 0,
    remark: ""

  }
  constructor(private router:Router,private vendorService: VendorService,private paymentService: PaymentService) {
    this.paymentForm = new FormGroup({});
    this.paymentForm.addControl('panNumber', new FormControl("", [Validators.required, Validators.maxLength(45)]));
    this.paymentForm.addControl('amount', new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]));
    this.paymentForm.addControl('frequency', new FormControl('', [Validators.required]));
    this.paymentForm.addControl('vendor', new FormControl('', [Validators.required]));
  }

  ngOnInit() {
    this.vendorService.getVendorListBasedOnType("Tax").subscribe((response:any)=>{
      this.vendors=response;
    })
  }

  sendValue() {
    this.taxValue['amount']=this.paymentForm.value.amount;
    this.taxValue['frequency']=this.paymentForm.value.frequency;
    this.vendors.forEach((element)=> {
      if(this.paymentForm.value.vendor==element.vendorName) {
        this.taxValue['vendor']=element;
      }
    });
    this.taxValue['panNumber']=this.paymentForm.value.panNumber;
    this.paymentService.setformData(this.taxValue);
    this.paymentService.formName="Tax";
    console.log(this.taxValue);
    this.router.navigate(["/gateway-options"]);
  }

}
