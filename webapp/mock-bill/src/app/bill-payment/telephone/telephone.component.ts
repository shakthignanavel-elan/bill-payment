import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControlName, FormControl, Validators } from '@angular/forms';
import { Vendor } from 'src/app/site/vendor';
import { VendorService } from 'src/app/admin/vendor.service';
import { PaymentService } from '../payment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-telephone',
  templateUrl: './telephone.component.html',
  styleUrls: ['./telephone.component.css']
})
export class TelephoneComponent implements OnInit {

  paymentForm: FormGroup;
  accountNumber: FormControlName;
  amount: FormControlName;
  frequency: FormControlName;
  vendor: FormControlName;
  vendors: Vendor[];
  telephoneValue: any={};
  registerStatus: boolean = true;
  userExists: boolean = false;
  vendor1: Vendor = {
    vendorId: 0,
    imageURL: "",
    address: "",
    contactNumber: "",
    country: "",
    email: "",
    establishedYear: 0,
    gateway: "",
    issuedDate: new Date(),
    password: "",
    state: "",
    validityDate: new Date(),
    vendorName: "",
    vendorRegNo: "",
    vendorType: "",
    website: "",
    status: 0,
    remark: ""

  }
  constructor(private vendorService: VendorService, private paymentService: PaymentService,private router:Router) {
    this.paymentForm = new FormGroup({});
    this.paymentForm.addControl('telephoneNumber', new FormControl("", [Validators.required, Validators.maxLength(12), Validators.minLength(12), Validators.pattern("^[0-9]*$")]));
    this.paymentForm.addControl('amount', new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]));
    this.paymentForm.addControl('frequency', new FormControl('', [Validators.required]));
    this.paymentForm.addControl('vendor', new FormControl('', [Validators.required]));
  }

  ngOnInit() {
    this.vendorService.getVendorListBasedOnType("Telephone").subscribe((response: any) => {
      this.vendors = response;
    })
  }

  sendValue() {
    this.telephoneValue['amount'] = this.paymentForm.value.amount;
    this.telephoneValue['frequency'] = this.paymentForm.value.frequency;
    this.vendors.forEach((element) => {
      if (this.paymentForm.value.vendor == element.vendorName) {
        this.telephoneValue['vendor'] = element;
      }
    });
    this.telephoneValue['telephoneNumber'] = this.paymentForm.value.telephoneNumber;
    this.paymentService.formData = this.telephoneValue;
    console.log(this.telephoneValue);
    this.paymentService.formName="Telephone";
    this.router.navigate(["/gateway-options"]);
  }
}
