import { Injectable } from '@angular/core';
import {environment} from '../../src/environments/environment'
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {User} from './site/user';
import { Vendor } from './site/vendor';
import { AuthenticationService } from './site/authentication.service';

@Injectable({
  providedIn: 'root'
})

export class EndpointService {

  // private authenticationApiUrl = environment.authenticationBaseUrl + '/authentication-service/bill-payment-authentication';
  // private billPaymentURL = environment.billpayBaseUrl+'/billpay-service/bill-payment';
  private authenticationApiUrl = environment.authenticationBaseUrl + '/bill-payment-authentication';
  private billPaymentURL = environment.billpayBaseUrl+'/bill-payment';

  constructor(private httpClient: HttpClient,private authenticationService: AuthenticationService) { }

  authenticate(user: string, password: string) {
    let authURL = this.authenticationApiUrl+"/authenticate";
    let credentials = btoa(user + ':' + password);
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Basic ' + credentials);
    return this.httpClient.get(authURL, { headers });
  }
  signupUser(user: User) {
    let authURL =  this.authenticationApiUrl+"/users";
    return this.httpClient.post(authURL,user);
  }
  countryEndpoint() {
    //let url = "http://battuta.medunes.net/api/country/all/?key={db5db1e7ff155b761691c8fbd390da39}"
    //let url="https://restcountries.eu/rest/v2/all"
    const httpOptions = {
      headers: new HttpHeaders({
        'x-requested-with': 'xhr'
      })
    }
    let url="https://cors-anywhere.herokuapp.com/http://lab.iamrohit.in/php_ajax_country_state_city_dropdown/apiv1.php?type=getCountries";
    return this.httpClient.get(url,httpOptions);
  }
  stateEndpoint(id:string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'x-requested-with': 'xhr'
      })
    }
    let url="https://cors-anywhere.herokuapp.com/http://lab.iamrohit.in/php_ajax_country_state_city_dropdown/apiv1.php?type=getStates&countryId="+"id";
    
    return this.httpClient.get(url,httpOptions);
  }
  signupVendor(vendor: Vendor) {
    let authURL =  this.authenticationApiUrl+"/vendors";
    return this.httpClient.post(authURL,vendor);
  }
  getAllVendors() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + this.authenticationService.getToken()
      })
    }
    console.log(this.authenticationService.getToken());
    var url: string = this.billPaymentURL+"/vendorList";
    return this.httpClient.get(url,httpOptions);
  }
  updateVendorStatus(vendor: Vendor) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + this.authenticationService.getToken()
      })
    }
    var url: string = this.billPaymentURL+"/update-vendor-status";
    return this.httpClient.put(url,vendor,httpOptions);
  }
  getVendorList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + this.authenticationService.getToken()
      })
    }
    var url: string = this.billPaymentURL+"/vendor/"+this.authenticationService.getUser();
    console.log(url);
    return this.httpClient.get(url,httpOptions);
  }
  editVendor(vendor: Vendor) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + this.authenticationService.getToken()
      })
    }
    var url: string = this.billPaymentURL+"/update-vendor";
    return this.httpClient.put(url,vendor,httpOptions);
  }
  getVendorListBasedOnType(type: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + this.authenticationService.getToken()
      })
    }
    var url:string = this.billPaymentURL+"/vendorTypes/"+type;
    return this.httpClient.get(url,httpOptions);
  }
  getUser(userName:String) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + this.authenticationService.getToken()
      })
    }
    var url:string = this.billPaymentURL+"/user/"+userName;
    console.log(url);
    return this.httpClient.get(url,httpOptions);
  }
  getBillList(userId:number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + this.authenticationService.getToken()
      })
    }
    var url:string = this.billPaymentURL+"/bill-details/getList/"+userId;
    console.log(url);
    return this.httpClient.get(url,httpOptions);
  }
  saveBill(billDetails:any,billType:number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + this.authenticationService.getToken()
      })
    }
    var url:string = this.billPaymentURL+"/bill-details/updateList/";
    switch(billType) {
      case 1: url=url+"credit-card";break;
      case 2: url=url+"dth";break;
      case 3: url=url+"electricity";break;
      case 4: url=url+"insurance";break;
      case 5: url=url+"loan";break;
      case 6: url=url+"tax";break;
      case 7: url=url+"telephone";break;
    }
    console.log("url is "+url);
    return this.httpClient.post(url,billDetails,httpOptions);
  }
}
