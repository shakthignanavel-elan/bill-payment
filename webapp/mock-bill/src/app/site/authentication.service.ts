import { Injectable, OnInit } from '@angular/core';
import { EndpointService } from '../endpoint.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements OnInit {
  ngOnInit(): void {
  }

  private token: string;
  private role: string = "anonymous";
  private user: string;
  private movieId: number;
  private userId: string;
  state: boolean;
  invalid: boolean = false;

  constructor() {
  }
  // authenticate(username, password) {
  //   this.endpointService.authenticate(username, password)
  //     .subscribe((response) => {
  //       this.token=response['token'];
  //       this.user = response['name'];
  //       let role=response['role'];
  //       if (role == "ADMIN") {
  //         this.role = "admin";
  //       } else if (this.role == "USER") {
  //         this.role = "customer";
  //       } else if (this.role == "VENDOR") {
  //         this.role = "vendor";
  //       }
  //       this.invalid = false;
  //       this.router.navigate(['food-menu']);
  //     },
  //       (error) => {
  //         this.invalid = true;
  //       }
  //     );

  // }

  setUserId(userId: string) {
    this.userId=this.userId;
  }
  getUserId() {
    return this.userId;
  }
  isInvalid() {
    return this.invalid;
  }
  login() {
    this.state = true;
  }

  logout() {
    this.state = false;
  }

  loggedInUser() {
    return this.state;
  }
  public setToken(token: string) {
    this.token = token;
  }
  public getToken() {
    return this.token;
  }

  public getRole() {
    return this.role;
  }
  public setRole(role: string) {
    this.role = role;
  }
  public getUser() {
    return this.user;
  }
  public setUser(user: string) {
    this.user = user;
  }
  public setMovieId(id: number) {
    this.movieId = id;
  }
  public getMovieId() {
    return this.movieId;
  }
  public getLoggedInUser() {
    return this.user;
  }
  userLoggedOut() {
    this.user = "anonymous";
    this.role = "anonymous";
    this.token = "";
  }
}
