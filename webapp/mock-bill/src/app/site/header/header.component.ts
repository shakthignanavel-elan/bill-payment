import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  loggedInUser: String;
  loggedInUserDetails: string;
  name: string;
  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.loggedInUser = this.authService.getRole();
    this.loggedInUserDetails = this.authService.getUser();
  }

  isAdmin() {
    let roleCheck = false;
    if (this.loggedInUser == "admin") {
      roleCheck = true;
    }
    return roleCheck;
  }

  isCustomer() {
    let roleCheck = false;
    if (this.loggedInUser == "customer") {
      roleCheck = true;
    }
    return roleCheck;
  }
  isAnonymous() {
    let roleCheck = false;
    if (this.authService.getRole() == "anonymous") {
      roleCheck = true;
    }
    return roleCheck;
  }

  getLoggedInUser() {
    return this.authService.getUser();
  }
  foodMenu() {
    this.router.navigate(['food-menu']);
  }
  cart() {
    this.router.navigate(['Cart']);
  }
  login() {
    this.router.navigate(['login']);
  }
  signup() {
    this.router.navigate(['registration']);
  }
  logout() {
    this.authService.userLoggedOut();
    this.router.navigate(['login']);
  }

}
