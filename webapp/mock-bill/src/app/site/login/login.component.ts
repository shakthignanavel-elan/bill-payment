import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import {EndpointService} from '../../endpoint.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  error: string;
  isLoggedIn: boolean;
  loginStatus: boolean;
  movieId: number;
  invalidData: boolean =false;

  constructor(private router: ActivatedRoute, private endpointService:EndpointService, private authenticationService: AuthenticationService, private route: Router) { }

  ngOnInit() {
    this.movieId = 0;
    this.loginStatus = true;
    this.isLoggedIn = true;
    this.router.paramMap.subscribe(params => {
      if (params.get('status')) {
        this.isLoggedIn = false;
      }
    });

    this.loginForm = new FormGroup({
      'userName': new FormControl('', [Validators.required]
      ),
      'passwrd': new FormControl('', [Validators.required]
      )
    });
  }

  onSubmit(loginForm) {

    this.endpointService.authenticate(loginForm.value.userName, loginForm.value.passwrd).subscribe((response:any) => {
      
      this.authenticationService.setToken(response.token);
      this.authenticationService.setUser(response.name);
      this.authenticationService.setRole(response.role);
      this.authenticationService.invalid=false;
      this.invalidData=false;
      // this.loginStatus = true;
      // this.authenticationService.login();
      if(response.role==="ADMIN") {
          this.authenticationService.setRole("admin");
          this.route.navigate(['/vendor-list']);
      } else if(response.role == "USER") {
        this.authenticationService.setRole("user");
        this.route.navigate(["/user-menu"]);
      } else if(response.role == "VENDOR") {
        console.log(response);
        this.authenticationService.setRole("vendor");
        this.route.navigate(["/vendor-details"])
      }
      
    },
      (error) => {
        this.loginStatus = false;
        this.authenticationService.invalid=true;
        this.invalidData=true;
        this.error = "Invalid username/password";
      });
  }
}
