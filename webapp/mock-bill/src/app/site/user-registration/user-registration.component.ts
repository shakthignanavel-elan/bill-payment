import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { FormGroup, FormControlName, FormControl, Validators } from '@angular/forms';
import { EndpointService } from 'src/app/endpoint.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  user: User = {
    username:"",
    firstName: "",
    lastName: "",
    age: 12,
    gender:"",
    contact: 0,
    panNumber: "",
    adhaarNumber: "",
    password: ""
  };
  error: String;
  registrationForm: FormGroup;
  firstName: FormControlName;
  lastName: FormControlName;
  age: FormControlName;
  gender: FormControlName;
  contactNumber: FormControlName;
  userID: FormControlName;
  password: FormControlName;
  userType: FormControlName;
  userCreated: boolean = false;
  registerStatus: boolean=true;
  userExists:boolean=false;

  constructor(private endpointService: EndpointService, private router: Router) { }

  ngOnInit() {
    this.userCreated = false;
    this.registrationForm = new FormGroup({});
    this.registrationForm.addControl('firstName',new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.registrationForm.addControl('lastName', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.registrationForm.addControl('age', new FormControl("", [Validators.required, Validators.maxLength(2), Validators.pattern("^[0-9]*$")]));
    this.registrationForm.addControl('gender', new FormControl("", [Validators.required]));
    this.registrationForm.addControl('contactNumber', new FormControl("", [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]));
    this.registrationForm.addControl('userID', new FormControl("", [Validators.required, Validators.maxLength(15), Validators.minLength(6)]));
    this.registrationForm.addControl('password', new FormControl("", [Validators.required, Validators.maxLength(15), Validators.minLength(6)]));
    this.registrationForm.addControl('panNumber', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.registrationForm.addControl('aadharNumber', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.registrationForm.addControl('cnfPwd', new FormControl("", [Validators.required, Validators.maxLength(15), Validators.minLength(2), Validators.compose([this.validateAreEqual.bind(this)])]));
  }

  private validateAreEqual(fieldControl: FormControl) {
    return fieldControl.value === this.registrationForm.value.password ? null : {
      'mismatch': true
    };
  }
  sendValue(){
    this.user.firstName = this.registrationForm.value.firstName;
    this.user.lastName = this.registrationForm.value.lastName;
    this.user.age = this.registrationForm.value.age;
    this.user.gender = this.registrationForm.value.gender;
    this.user.contact = this.registrationForm.value.contactNumber;
    this.user.username = this.registrationForm.value.userID;
    this.user.password = this.registrationForm.value.password;
    this.user.username = this.registrationForm.value.userID;
    this.user.panNumber = this.registrationForm.value.panNumber;
    this.user.adhaarNumber=this.registrationForm.value.aadharNumber;
    this.endpointService.signupUser(this.user).subscribe((response)=>{
      this.router.navigate(['login']);
      this.registerStatus=true;
    },(errors)=>{
      this.error="Username already registered. Register with a different Username";
      this.registerStatus=false;
    });
  }
}
