export interface User {
    username: string;
    firstName: string;
    lastName: string;
    age: number;
    gender:string;
    contact: number;
    panNumber: string;
    adhaarNumber: string;
    password: string;
}