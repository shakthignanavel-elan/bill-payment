import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControlName, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { EndpointService } from '../../endpoint.service';
import { Vendor } from '../vendor';
import { Router } from '@angular/router';
@Component({
  selector: 'app-vendor-registration',
  templateUrl: './vendor-registration.component.html',
  styleUrls: ['./vendor-registration.component.css']
})
export class VendorRegistrationComponent implements OnInit {

  registrationForm: FormGroup;
  vendorName: FormControlName;
  vendorRegNo: FormControlName;
  addrLine1: FormControlName;
  addrLine2: FormControlName;
  country: FormControlName;
  state: FormControlName;
  email: FormControlName;
  contactNum: FormControlName;
  website: FormControlName;
  issuedDate: FormControlName;
  validityDate: FormControlName;
  establishedYr: FormControlName;
  vendorType: FormControlName;
  gateway: FormControlName;
  pwd: FormControlName;
  cnfPwd: FormControlName;
  userCreated: boolean = false;
  countryResponse: boolean = false;
  dataLoaded: Promise<boolean>;
  countries: any;
  numbers: number[];
  countriesMap: Map<Number, String>;
  vendor: Vendor = {
    vendorId: 23,
    vendorName: "",
    vendorRegNo: "",
    vendorType: "",
    address: "",
    country: "",
    state: "",
    email: "",
    contactNumber: "",
    website: "",
    issuedDate: new Date(),
    validityDate: new Date(),
    establishedYear: new Date().getFullYear(),
    gateway: "",
    password: "",
    imageURL: "",
    status: 0,
    remark: ""
  };
  constructor(private httpClient: HttpClient, private endpointService: EndpointService,private router: Router) { }

  ngOnInit() {
    this.registrationForm = new FormGroup({});
    this.countriesMap = new Map();
    this.registrationForm.addControl('vendorName', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.registrationForm.addControl('vendorRegNo', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.registrationForm.addControl('addrLine', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.registrationForm.addControl('imageURL', new FormControl("", [Validators.required, Validators.maxLength(500), Validators.minLength(2)]));
    this.registrationForm.addControl('contactNum', new FormControl("", [Validators.required, Validators.maxLength(12), Validators.minLength(12), Validators.pattern("^[0-9]*$")]));
    this.registrationForm.addControl('country', new FormControl("", [Validators.required]));
    this.registrationForm.addControl('email', new FormControl("", [Validators.email, Validators.required]));
    this.registrationForm.addControl('website', new FormControl("", [Validators.required, Validators.maxLength(15), Validators.minLength(2)]));
    this.registrationForm.addControl('issuedDate', new FormControl("", [Validators.required]));
    this.registrationForm.addControl('validityDate', new FormControl("", [Validators.required]));
    this.registrationForm.addControl('establishedYr', new FormControl("", [Validators.required]));
    this.registrationForm.addControl('vendorType', new FormControl("", [Validators.required]));
    this.registrationForm.addControl('pwd', new FormControl("", [Validators.required, Validators.maxLength(15), Validators.minLength(2)]));
    this.registrationForm.addControl('cnfPwd', new FormControl("", [Validators.required, Validators.maxLength(15), Validators.minLength(2), Validators.compose([this.validateAreEqual.bind(this)])]));
    this.registrationForm.addControl('gateway', new FormControl("", [Validators.required, Validators.maxLength(15), Validators.minLength(2)]));
    this.registrationForm.addControl('state', new FormControl("", [Validators.required, Validators.maxLength(50), Validators.minLength(2)]));
    this.getCountry();
    let date: number = new Date().getFullYear() - 999;
    this.numbers = Array(1000).fill(0).map((x, i) => i + date);

  }
  private validateAreEqual(fieldControl: FormControl) {
    return fieldControl.value === this.registrationForm.value.pwd ? null : {
      'mismatch': true
    };
  }
  passEquals() {
    if (this.registrationForm.value.pwd === this.registrationForm.value.cnfPwd) {
      //this.registrationForm.valid=false;
      return false;
    } else {
      return true;
    }
  }
  getCountry() {
    const endpointCountry = this.endpointService.countryEndpoint().subscribe((response) => {
      this.countriesMap = response['result'];
      this.countryResponse = true;
      this.dataLoaded = Promise.resolve(true);
      this.countries = response;
      //return response;
    });
    if (this.countryResponse == true) {
      endpointCountry.unsubscribe();
    }
  }

  sendValue() {
    this.vendor.vendorName = this.registrationForm.value.vendorName;
    this.vendor.vendorRegNo = this.registrationForm.value.vendorRegNo;
    this.vendor.address = this.registrationForm.value.addrLine;
    this.vendor.imageURL=this.registrationForm.value.imageURL;
    this.vendor.contactNumber = this.registrationForm.value.contactNum;
    this.vendor.country = this.registrationForm.value.country;
    this.vendor.email = this.registrationForm.value.email;
    this.vendor.website = this.registrationForm.value.website;
    this.vendor.issuedDate = this.registrationForm.value.issuedDate;
    this.vendor.validityDate = this.registrationForm.value.validityDate;
    this.vendor.establishedYear = this.registrationForm.value.establishedYr;
    this.vendor.vendorType = this.registrationForm.value.vendorType;
    this.vendor.gateway = this.registrationForm.value.gateway;
    this.vendor.state = this.registrationForm.value.state;
    this.vendor.password = this.registrationForm.value.pwd;
    this.vendor.status=0;
    this.endpointService.signupVendor(this.vendor).subscribe((response)=>{
      this.router.navigate(['login']);
    },(errors)=>{
    });
  }

}
