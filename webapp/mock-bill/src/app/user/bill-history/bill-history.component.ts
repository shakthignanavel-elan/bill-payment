import { Component, OnInit } from '@angular/core';
import { Vendor } from 'src/app/site/vendor';

@Component({
  selector: 'app-bill-history',
  templateUrl: './bill-history.component.html',
  styleUrls: ['./bill-history.component.css']
})
export class BillHistoryComponent implements OnInit {

  billDetails: any[];
  bill:any;
  billType:any;
  Mockbill:any={};
  buttonClicked:boolean=false;
  vendor: Vendor = {
    vendorId: 0,
    imageURL: "",
    address: "",
    contactNumber: "",
    country: "",
    email: "",
    establishedYear: 0,
    gateway: "",
    issuedDate: new Date(),
    password: "",
    state: "",
    validityDate: new Date(),
    vendorName: "",
    vendorRegNo: "",
    vendorType: "",
    website: "",
    status: 0,
    remark: ""

  }
  constructor() { }

  ngOnInit() {
    this.vendor.imageURL = "https://www.google.co.in/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png",
      this.vendor.address = "dubai,dubai main,dubai",
      this.vendor.contactNumber = "12345678912",
      this.vendor.country = "India",
      this.vendor.email = "12@12.com",
      this.vendor.establishedYear = 1500,
      this.vendor.gateway = "pay",
      this.vendor.issuedDate = new Date(),
      this.vendor.password = "pwd",
      this.vendor.state = "assam",
      this.vendor.validityDate = new Date(),
      this.vendor.vendorName = "helloi",
      this.vendor.vendorRegNo = "123sd123",
      this.vendor.vendorType = "electricity",
      this.vendor.website = "www.www.com",
      this.vendor.status = 1
    this.vendor.vendorId = 2
    this.Mockbill['vendor']=this.vendor;
    this.Mockbill['id']=2;
    this.Mockbill['date']= new Date();
    this.Mockbill['frequency']=2;
    this.Mockbill['paymentType']="Credit card";
    this.Mockbill['amount']=1000;
    this.Mockbill['cardNumber']=2111275855;
    this.billDetails=[this.Mockbill,this.Mockbill];
    console.log(this.billDetails);
  }

  setBillType($event) {
    this.billType=1;
  }
  reviewVendor($event) {
    console.log(this.billDetails);
    console.log("hiii");
    this.buttonClicked=true
    this.billDetails.forEach((element)=>{
      if(element.vendor.vendorType=="electricity") {
        if(element.id==element.id) {
          this.bill=element;
          stop;
        }
      }
    });
    console.log(this.bill);

  }
}
