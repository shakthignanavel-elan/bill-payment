import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PaymentService } from 'src/app/bill-payment/payment.service';
import { Vendor } from 'src/app/site/vendor';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.css']
})
export class BillComponent implements OnInit {

  constructor(private paymentService: PaymentService) { }

  @Input() bill: any;
  @Output() viewDetailsEmitter = new EventEmitter();
  @Output() billTypeEmitter = new EventEmitter();
  ngOnInit() {
    console.log("sacascas")
    console.log(this.bill.vendor.vendorName)
  }
  review(id: number) {
    this.viewDetailsEmitter.emit(id);
    this.billTypeEmitter.emit(this.bill.vendor.vendorType);
  }

}
