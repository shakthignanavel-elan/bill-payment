import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EndpointService } from 'src/app/endpoint.service';
import { AuthenticationService } from 'src/app/site/authentication.service';
import { PaymentService } from 'src/app/bill-payment/payment.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  dataLoaded:Promise<Boolean>;
  constructor(private router: Router,private endpointService:EndpointService,private authenticationService: AuthenticationService,private paymentService:PaymentService) { }

  ngOnInit() {
    this.endpointService.getUser(this.authenticationService.getUser()).subscribe((response:any)=>{
     // this.paymentService.userDetails=response;
      console.log(response)
      console.log(this.paymentService.userDetails);
      this.paymentService.userDetails=response;
      this.dataLoaded=Promise.resolve(true);
      this.endpointService.getBillList(response.id).subscribe((response:any)=>{
        console.log(response);
      },(errors)=>{
        console.log(errors);
      })
    },(error)=>{
      console.log(error);
    })
  }

  electricity() {
    this.router.navigate(['electricity-payment']);
  }
  dth() {
    this.router.navigate(['dth-payment']);
  }
  telephone() {
    this.router.navigate(['telephone-payment']);
  }
  tax() {
    this.router.navigate(['tax-payment']);
  }
  insurance() {
    this.router.navigate(['insurance-payment']);
  }
  creditCard() {
    this.router.navigate(['creditcard-payment']);
  }
  loan() {
    this.router.navigate(['loan-payment']);
  }
  showAlert() {
    console.log("hello");
  }
}
